/*
 * ublox.h
 *
 *  Created on: 29 sep. 2021
 *      Author: juany
 */

#ifndef UBLOX_H_
#define UBLOX_H_
/** \addtogroup UBlox RTK Base-Rover
 ** @{ */

/* \brief Driver to use UBlox ZED F9P to send or receive RTK sentences
 *
 */

/*
 * Initials     Name
 * ---------------------------
 *	JAP			Juan Antonio Pighin
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211031 v0.1 initials initial version JAP
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/
typedef struct{
	uint8_t latDeg;
	uint8_t latMin;
	uint32_t latMinDec;
	uint32_t latDir;

	uint8_t lonDeg;
	uint8_t lonMin;
	uint32_t lonMinDec;
	uint32_t lonDir;

	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint8_t valid;

}UBLOX_GEO;

typedef struct __attribute__((packed)){
	uint16_t cntRTCM1005;
	uint16_t cntRTCM1074;
	uint16_t cntRTCM1084;
	uint16_t cntRTCM1094;
	uint16_t cntRTCM1124;
	uint16_t cntRTCM1230;
	uint16_t cntRTCM4072;
	uint16_t crcError;
}UBLOX_RTCM_STATE;


typedef struct {
	uint8_t version;
	uint8_t reserved[3];
	uint32_t iTOW;
	uint32_t dur;
	int32_t meanX;
	int32_t meanY;
	int32_t meanZ;
	int8_t meanXHP;
	int8_t meanYHP;
	int8_t meanZHP;
	int8_t reserved1;
	uint32_t meanAcc;
	uint32_t obs;
	uint8_t valid;
	uint8_t active;
	uint8_t reserved2[2];
}UBLOX_NAV_SVIN_SATE;

typedef struct {
	uint32_t surveyInTime;
	uint32_t surveyInPrec;
	void *pRTKBroadcast;			/*!< Function pointer to app send rtk*/

}UBLOX_BASE_STRUCT ;


typedef struct {
	uint32_t surveyInTime;
	uint32_t surveyInPrec;
	void *pRTKBroadcast;			/*!< Function pointer to app send rtk*/
}UBLOX_ROVER_STRUCT ;

typedef struct{
	uint16_t NAV_SIG;
	uint16_t NAV_PVT;
	uint16_t NAV_POSLLH;
	uint16_t NAV_RELPOSNED;
	uint16_t NAV_STATUS;
	uint16_t RXM_RTCM;
}UBLOX_ROVER_MSGS;

typedef struct{
	uint8_t version;
	uint8_t crcFailed:1;
	uint8_t msgUsed:2;
	uint8_t reserved:5;
	uint16_t subType;
	uint16_t refStation;
	uint16_t msgType;
}UBLOX_RXM_RTCM;

typedef struct{
	uint32_t iTOW;
	uint8_t gpsFix;
	uint8_t gpsFixOk:1;
	uint8_t diffSoIn:1;
	uint8_t wknSet:1;
	uint8_t towSet:1;
	uint8_t rscd:4;
	uint8_t diffCorr:1;
	uint8_t carrSoInValid:1;
	uint8_t rsvd2:4;
	uint8_t mapMatching:2;
	uint8_t psmState:2;
	uint8_t rsvd3:1;
	uint8_t spoofDetState:2;
	uint8_t rsvd4:1;
	uint8_t carrSoIn:2;
	uint32_t ttff;
	uint32_t msss;
}UBLOX_UBX_STATUS;

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/

/* @fn void UBloxBaseInit(UBLOX_BASE_STRUCT ubloxBase);
 * @brief Initialize the module as base station
 * @param[in] ubloxBase UBLOX_BASE_STRUCT with the necesary data to iunitialize the base mode
 */
void UBloxBaseInit(UBLOX_BASE_STRUCT ubloxBase);

/* @fn void UBloxRoverInit(void);
 * @brief Initialize the module as rover
 */
void UBloxRoverInit(void);

/* @fn uint8_t UBloxRTCMProcesss(uint8_t rc)
 * @brief Process a byte included on an RTCM buffer received from broadcast
 * @param[in] rc byte received
 * @return whether the buffer was fully processed or not
 */
uint8_t UBloxRTCMProcesss(uint8_t rc)

/* @fn UBLOX_RTCM_STATE *UBloxGetRTCMSentences(void)
 * @brief Getter for the RTCM sentences received counters
 * @return struct with the received rtcm sentences
 */
UBLOX_RTCM_STATE *UBloxGetRTCMSentences(void);

/* @fn UBLOX_GEO *UBloxGetGeoData(void)
 * @brief Getter for the Geo data
 * @return struct with the Geo Data
 */
UBLOX_GEO *UBloxGetGeoData(void);

// Base functions

/* @fn UBLOX_NAV_SVIN_SATE *UBloxGetSurveyStatus(void)
 * @brief Getter for the survey status struct
 * @return struct with the current server status
 */
UBLOX_NAV_SVIN_SATE *UBloxGetSurveyStatus(void);

// ROVER functions
/* @fn UBLOX_ROVER_MSGS *UBloxGetRoverMsgs(void)
 * @brief Getter for the rover msgs struct
 * @return struct with the rover messages count
 */
UBLOX_ROVER_MSGS *UBloxGetRoverMsgs(void);

/* @fn UBLOX_UBX_STATUS *UBloxGetRoverStatus(void)
 * @brief Getter for the rover status
 * @return struct with the rover status
 */
UBLOX_UBX_STATUS *UBloxGetRoverStatus(void);

#endif /* UBLOX_H_ */
