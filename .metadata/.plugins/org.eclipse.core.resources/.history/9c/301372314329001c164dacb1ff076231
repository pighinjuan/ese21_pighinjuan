/*
 * ublox.c
 *
 *  Created on: 29 sep. 2021
 *      Author: juany
 */

#include "uart.h"
#include "led.h"
#include <stdint.h>
#include "console.h"

uint32_t crc24q(const uint8_t *buf, uint32_t len, uint32_t crc) ;

uint8_t headBuf[16];
uint8_t bodyBuf[256];
uint8_t chkBuf[64];

typedef struct __attribute__((packed)){
	uint16_t preamble;
	uint8_t class;
	uint8_t ID;
	uint8_t len;
//	uint8_t *payload;
//	uint8_t *checksum;
}UBX_HEAD_STRUCT;




struct {
	uint8_t RTCM1074:1;
	uint8_t rsvd:7;
}ackedRTCM;

void uBloxProcessData(uint8_t rc);
void uBloxProcessNMEA(void);
void uBloxSendUBXConfig1074(void);
void uBloxProcessUBX(void);
void uBloxProcessRTCM(void);

enum {
	UBLOX_WAITING_SOF,
	UBLOX_NMEA_READING_HEADER,
	UBLOX_NMEA_READING_BODY,
	UBLOX_NMEA_READING_CHECKSUM,
	UBLOX_RTCM_READING_HEADER,
	UBLOX_RTCM_READING_BODY,
	UBLOX_RTCM_READING_CHECKSUM,
	UBLOX_UBX_READING_HEADER,
	UBLOX_UBX_READING_BODY,
	UBLOX_UBX_READING_CHECKSUM,
	UBLOX_FAIL
}uBloxReadState;

uint8_t unaTrama[] = {	0xD3,0x00,0x7C,0x44,0x90,0x00,0x65,0x9C,0x73,0x82,0x00,0x00,0x2C,0x40,0x00,0x42,
						0x08,0x00,0x00,0x00,0x20,0x00,0x00,0x00,0x7F,0x51,0x5A,0x5B,0x4F,0x57,0x56,0x50,
						0x00,0x00,0x00,0x0E,0xED,0x25,0x57,0x76,0xD2,0xC9,0xF7,0x52,0x7F,0x3C,0xFE,0xD4,
						0x21,0x10,0x58,0xBD,0xE1,0x04,0x2F,0xFF,0x3C,0x6F,0x6D,0x1D,0xB4,0xA0,0xE2,0xAD,
						0xF5,0x80,0xBD,0xF4,0x63,0xD3,0x15,0x42,0x4E,0xE1,0xF2,0x11,0x1D,0x06,0xD5,0xDC,
						0x03,0xC1,0xA7,0xFE,0x7B,0x9D,0xF7,0xBE,0x09,0xF4,0xE5,0xDE,0x08,0xC9,0x91,0x78,
						0x5A,0xA6,0x31,0x4E,0x68,0xDD,0xC5,0xA4,0x05,0x20,0xF8,0x40,0x0F,0x84,0x81,0x08,
						0x42,0x1F,0x34,0x6E,0x2F,0x09,0x50,0x4B,0x23,0xC6,0xE0,0xDF,0x5E,0x48,0xF0,0x52,
						0xD2,0x18};

void uBloxReceiveData(void){
	uint8_t readVal;
	if (UartReadByte(SERIAL_PORT_P2_CONNECTOR,&readVal)){
		LedToggle(2);
		uBloxProcessData(readVal);
	}
}


void uBloxProcessData(uint8_t rc){
	static uint8_t *bufptr = headBuf;
	static uint8_t bufCnt = 0;
	static uint8_t rtcmLen = 0;
	static uint32_t rtcmCRC = 0;

	static uint8_t ubxLen = 0;
	static uint8_t ubxChkA = 0;
	static uint8_t ubxChkB = 0;




	bufptr[bufCnt] = rc;
	bufCnt++;

	switch (uBloxReadState){
		case UBLOX_WAITING_SOF:
			if (rc == '$'){	// Nueva trama NMEA
				uBloxReadState = UBLOX_NMEA_READING_HEADER;
			}
			else if (rc == 0xD3){	// Nueva trama RTCM
				uBloxReadState = UBLOX_RTCM_READING_HEADER;
				rtcmCRC = 0;
				rtcmCRC = crc24q(&rc, 1, rtcmCRC);
				rtcmLen = 0;
			}
			else if (rc == 0xB5){
				// Posible primer byte de la trama UBX, esperamos al segundo 0x62
			}
			else if (rc == 0x62 && bufCnt == 2 && headBuf[0] == 0xB5) {	// Nueva trama UBX
				uBloxReadState = UBLOX_UBX_READING_HEADER;
				ubxChkA = 0;
				ubxChkB = 0;
				ubxLen = 0;
			}
			else {
				bufptr = headBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_NMEA_READING_HEADER:
			if (rc == ','){
				uBloxReadState = UBLOX_NMEA_READING_BODY;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = bodyBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_NMEA_READING_BODY:
			if (rc == '*'){
				uBloxReadState = UBLOX_NMEA_READING_CHECKSUM;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = chkBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_NMEA_READING_CHECKSUM:
			if (bufCnt == 2){	// 2 Bytes de checksum
				uBloxReadState = UBLOX_WAITING_SOF;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = headBuf;
				bufCnt = 0;
				uBloxProcessNMEA();
			}
			break;
		case UBLOX_RTCM_READING_HEADER:
			rtcmCRC = crc24q(&rc, 1, rtcmCRC);
			if (bufCnt == 3){	// 3 Byte de header
				uBloxReadState = UBLOX_RTCM_READING_BODY;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				rtcmLen = (headBuf[1] & 0x03) * 256 + headBuf[2];
				bufptr = bodyBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_RTCM_READING_BODY:
			rtcmCRC = crc24q(&rc, 1, rtcmCRC);
			if (bufCnt == rtcmLen){	// La longitud la da el header.
				uBloxReadState = UBLOX_RTCM_READING_CHECKSUM;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = chkBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_RTCM_READING_CHECKSUM:
			if (bufCnt == 3){	// 3 bytes de CRC
				uint32_t crc;
				uBloxReadState = UBLOX_WAITING_SOF;
				crc = (((((uint32_t) chkBuf[0]) * 256 ) + chkBuf[1]) * 256 ) + chkBuf[0];
				if (crc == rtcmCRC){
					uBloxProcessRTCM();
				}
				else{
					consoleSendString("\nTRCM CRC Error!");
				}
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = headBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_UBX_READING_HEADER:
			ubxChkA = ubxChkA + rc;
			ubxChkB = ubxChkB + ubxChkA;
			if (bufCnt == 6){
				uBloxReadState = UBLOX_UBX_READING_BODY;
				ubxLen = (headBuf[5]) * 256 + headBuf[4];
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = bodyBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_UBX_READING_BODY:
			ubxChkA = ubxChkA + rc;
			ubxChkB = ubxChkB + ubxChkA;
			if (bufCnt == ubxLen){
				uBloxReadState = UBLOX_UBX_READING_CHECKSUM;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = chkBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_UBX_READING_CHECKSUM:
			if (bufCnt == 2){
				uBloxReadState = UBLOX_WAITING_SOF;
				if (chkBuf[0] == ubxChkA && chkBuf[1] == ubxChkB ){
					uBloxProcessUBX();

				}
				else{
					consoleSendString("\nUBX Checksum Error!");
					ackedRTCM.RTCM1074 = 1;
				}
				bufptr = headBuf;
				bufCnt = 0;


			}
			break;
	}
}

void ubloxInit(void){
	serial_config serialinit = {SERIAL_PORT_P2_CONNECTOR,38400,uBloxReceiveData};
	uint32_t crc;

	UartInit(&serialinit);




	uBloxSendUBXConfig1074();


//	crc = crc24q(unaTrama,127,0);

	uBloxReadState = UBLOX_WAITING_SOF;
}



static const uint32_t crc24qtab[256] = {
		0x000000, 0x864CFB, 0x8AD50D, 0x0C99F6, 0x93E6E1, 0x15AA1A, 0x1933EC, 0x9F7F17,
		0xA18139, 0x27CDC2, 0x2B5434, 0xAD18CF, 0x3267D8, 0xB42B23, 0xB8B2D5, 0x3EFE2E,
		0xC54E89, 0x430272, 0x4F9B84, 0xC9D77F, 0x56A868, 0xD0E493, 0xDC7D65, 0x5A319E,
		0x64CFB0, 0xE2834B, 0xEE1ABD, 0x685646, 0xF72951, 0x7165AA, 0x7DFC5C, 0xFBB0A7,
		0x0CD1E9, 0x8A9D12, 0x8604E4, 0x00481F, 0x9F3708, 0x197BF3, 0x15E205, 0x93AEFE,
		0xAD50D0, 0x2B1C2B, 0x2785DD, 0xA1C926, 0x3EB631, 0xB8FACA, 0xB4633C, 0x322FC7,
		0xC99F60, 0x4FD39B, 0x434A6D, 0xC50696, 0x5A7981, 0xDC357A, 0xD0AC8C, 0x56E077,
		0x681E59, 0xEE52A2, 0xE2CB54, 0x6487AF, 0xFBF8B8, 0x7DB443, 0x712DB5, 0xF7614E,
		0x19A3D2, 0x9FEF29, 0x9376DF, 0x153A24, 0x8A4533, 0x0C09C8, 0x00903E, 0x86DCC5,
		0xB822EB, 0x3E6E10, 0x32F7E6, 0xB4BB1D, 0x2BC40A, 0xAD88F1, 0xA11107, 0x275DFC,
		0xDCED5B, 0x5AA1A0, 0x563856, 0xD074AD, 0x4F0BBA, 0xC94741, 0xC5DEB7, 0x43924C,
		0x7D6C62, 0xFB2099, 0xF7B96F, 0x71F594, 0xEE8A83, 0x68C678, 0x645F8E, 0xE21375,
		0x15723B, 0x933EC0, 0x9FA736, 0x19EBCD, 0x8694DA, 0x00D821, 0x0C41D7, 0x8A0D2C,
		0xB4F302, 0x32BFF9, 0x3E260F, 0xB86AF4, 0x2715E3, 0xA15918, 0xADC0EE, 0x2B8C15,
		0xD03CB2, 0x567049, 0x5AE9BF, 0xDCA544, 0x43DA53, 0xC596A8, 0xC90F5E, 0x4F43A5,
		0x71BD8B, 0xF7F170, 0xFB6886, 0x7D247D, 0xE25B6A, 0x641791, 0x688E67, 0xEEC29C,
		0x3347A4, 0xB50B5F, 0xB992A9, 0x3FDE52, 0xA0A145, 0x26EDBE, 0x2A7448, 0xAC38B3,
		0x92C69D, 0x148A66, 0x181390, 0x9E5F6B, 0x01207C, 0x876C87, 0x8BF571, 0x0DB98A,
		0xF6092D, 0x7045D6, 0x7CDC20, 0xFA90DB, 0x65EFCC, 0xE3A337, 0xEF3AC1, 0x69763A,
		0x578814, 0xD1C4EF, 0xDD5D19, 0x5B11E2, 0xC46EF5, 0x42220E, 0x4EBBF8, 0xC8F703,
		0x3F964D, 0xB9DAB6, 0xB54340, 0x330FBB, 0xAC70AC, 0x2A3C57, 0x26A5A1, 0xA0E95A,
		0x9E1774, 0x185B8F, 0x14C279, 0x928E82, 0x0DF195, 0x8BBD6E, 0x872498, 0x016863,
		0xFAD8C4, 0x7C943F, 0x700DC9, 0xF64132, 0x693E25, 0xEF72DE, 0xE3EB28, 0x65A7D3,
		0x5B59FD, 0xDD1506, 0xD18CF0, 0x57C00B, 0xC8BF1C, 0x4EF3E7, 0x426A11, 0xC426EA,
		0x2AE476, 0xACA88D, 0xA0317B, 0x267D80, 0xB90297, 0x3F4E6C, 0x33D79A, 0xB59B61,
		0x8B654F, 0x0D29B4, 0x01B042, 0x87FCB9, 0x1883AE, 0x9ECF55, 0x9256A3, 0x141A58,
		0xEFAAFF, 0x69E604, 0x657FF2, 0xE33309, 0x7C4C1E, 0xFA00E5, 0xF69913, 0x70D5E8,
		0x4E2BC6, 0xC8673D, 0xC4FECB, 0x42B230, 0xDDCD27, 0x5B81DC, 0x57182A, 0xD154D1,
		0x26359F, 0xA07964, 0xACE092, 0x2AAC69, 0xB5D37E, 0x339F85, 0x3F0673, 0xB94A88,
		0x87B4A6, 0x01F85D, 0x0D61AB, 0x8B2D50, 0x145247, 0x921EBC, 0x9E874A, 0x18CBB1,
		0xE37B16, 0x6537ED, 0x69AE1B, 0xEFE2E0, 0x709DF7, 0xF6D10C, 0xFA48FA, 0x7C0401,
		0x42FA2F, 0xC4B6D4, 0xC82F22, 0x4E63D9, 0xD11CCE, 0x575035, 0x5BC9C3, 0xDD8538
};

uint32_t crc24q(const uint8_t *buf, uint32_t len, uint32_t crc) {
	uint32_t i;
	for (i = 0; i < len; i++)
		crc = ((crc << 8) & 0xFFFFFF) ^ crc24qtab[(crc >> 16) ^ buf[i]];
	return crc;
}

void uBloxProcessNMEA(void) {
	consoleSendString("\n");
	consoleSendString((char *) headBuf);
	consoleSendString((char *) bodyBuf);
	consoleSendString((char *) chkBuf);
}
void uBloxProcessRTCM(void){
	uint16_t rtcmLen = (((uint16_t)headBuf[1]) & 0x03) *256 + headBuf[2];

	if (((uint16_t) bodyBuf[0]) * 16 + bodyBuf[0] == 0x432){
		consoleSendString("\nRTCM 1074 received (");
		consoleSendString((char *) UartItoa(rtcmLen,10));
		consoleSendString(" bytes)");
	}
}

void uBloxProcessUBX(void){
	UBX_HEAD_STRUCT *ubxHeadStruct = (UBX_HEAD_STRUCT *) headBuf;

	if (ubxHeadStruct->class == 0x05){	// ACK
		if (bodyBuf[0] == 0x06 && bodyBuf[1] == 0x8A){
			ackedRTCM.RTCM1074 = 1;

			if (ubxHeadStruct->ID == 0x01){	// ACKED
				consoleSendString("\nACK OK");
			}
			else if (ubxHeadStruct->ID == 0x00){
				consoleSendString("\nACK FAIL");
			}
		}

	}
}

void uBloxSendUBXConfig1074(void){
	uint8_t buf[128];

	// Preambulo
	buf[0] = 0xB5;
	buf[1] = 0x62;

	// Class
	buf[2] = 0x06;
	// ID
	buf[3] = 0x8A;

	// Len
	buf[4] = 0x09;
	buf[5] = 0x00;

	// Version
	buf[6] = 0x00;
	// Bitfield
	buf[7] = 0x01;	// Solo Ram

	// Rsvd
	buf[8] = 0x00;
	buf[9] = 0x00;

	// Rsvd
	buf[8] = 0x00;
	buf[9] = 0x00;

	//MGOUT ID
	buf[10] = 0x5f;
	buf[11] = 0x03;
	buf[12] = 0x91;
	buf[13] = 0x20;

	//Val
	buf[14] = 0x01;

	// Checksum
	buf[15] = 0xAE;
	buf[16] = 0x0C;


	UartSendBuffer(SERIAL_PORT_P2_CONNECTOR,buf,17);

	consoleSendString("\nRTCM1074 Config sent");

	while(ackedRTCM.RTCM1074 == 0);

}
