/*
 * ublox.h
 *
 *  Created on: 29 sep. 2021
 *      Author: juany
 */

#ifndef UBLOX_H_
#define UBLOX_H_

#include <stdint.h>


typedef struct{
	uint8_t latDeg;
	uint8_t latMin;
	uint32_t latMinDec;

	uint8_t lonDeg;
	uint8_t lonMin;
	uint32_t lonMinDec;

	uint8_t hour;
	uint8_t min;
	uint8_t sec;
	uint8_t valid;

}UBLOX_GEO;

typedef struct __attribute__((packed)){
	uint16_t cntRTCM1005;
	uint16_t cntRTCM1074;
	uint16_t cntRTCM1084;
	uint16_t cntRTCM1094;
	uint16_t cntRTCM1124;
	uint16_t cntRTCM1230;
	uint16_t cntRTCM4072;
}UBLOX_RTCM_STATE;


typedef struct {
	uint8_t version;
	uint8_t reserved[3];
	uint32_t iTOW;
	uint32_t dur;
	int32_t meanX;
	int32_t meanY;
	int32_t meanZ;
	int8_t meanXHP;
	int8_t meanYHP;
	int8_t meanZHP;
	int8_t reserved1;
	uint32_t meanAcc;
	uint32_t obs;
	uint8_t valid;
	uint8_t active;
	uint8_t reserved2[2];
}UBLOX_NAV_SVIN_SATE;

typedef struct {
	uint32_t surveyInTime;
	uint32_t surveyInPrec;
	void *pRTKBroadcast;			/*!< Function pointer to app send rtk*/

}UBLOX_BASE_STRUCT ;


typedef struct {
	uint32_t surveyInTime;
	uint32_t surveyInPrec;
	void *pRTKBroadcast;			/*!< Function pointer to app send rtk*/
}UBLOX_ROVER_STRUCT ;

typedef struct{
	uint16_t NAV_SIG;
	uint16_t NAV_PVT;
	uint16_t NAV_POSLLH;
	uint16_t NAV_RELPOSNED;
	uint16_t NAV_STATUS;
	uint16_t RXM_RTCM;
}UBLOX_ROVER_MSGS;

typedef struct{
	uint8_t version;
	uint8_t crcFailed:1;
	uint8_t msgUsed:2;
	uint8_t reserved:5;
	uint16_t subType;
	uint16_t refStation;
	uint16_t msgType;
}UBLOX_RXM_RTCM;

void UbloxBaseInit(UBLOX_BASE_STRUCT ubloxBase);
void UbloxRoverInit(UBLOX_BASE_STRUCT ubloxBase);

void *UBloxGetRTCMProcessFunct(void);

//Base
UBLOX_RTCM_STATE *UBloxGetRTCMSentences(void);
UBLOX_NAV_SVIN_SATE *UBloxGetSurveyStatus(void);

// ROVER
UBLOX_ROVER_MSGS *UBloxGetRoverMsgs(void);
UBLOX_GEO *UBloxGetGeoData(void);


#endif /* UBLOX_H_ */
