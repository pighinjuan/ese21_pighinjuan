#include "RTKBroadcast.h"
#include "systemclock.h"
#include "led.h"
#include "timer.h"
#include "ublox.h"
#include "console.h"
#include "xbee.h"
#include "uart.h"
#include "meteo.h"
#include <stdio.h>
#include "string.h"
#include <stdlib.h>



/*==================[macros and definitions]=================================*/

#define BASE_MODE 1		// Modificar solo este!
#if BASE_MODE
#define ROVER_MODE 0	//<- NO TOCAR!
#else
#define ROVER_MODE 1	//<- NO TOCAR!
#endif

#define SENDBUF_SIZE 5

/*==================[external data declaration]==============================*/

/*==================[internal data declaration]=========================*/

/*==========================[typedefs]==========================*/
typedef struct{
	uint8_t  *pBuff;
	uint16_t bufLen;
}SENDBUF;

/*==================[internal functions declaration]=========================*/
/* @fn void ConsoleTask(void);
 * @brief Updates console data
 */
void ConsoleTask(void);

/* @fn void MeteoTask(void);
 * @brief updates meteo data
 */
void MeteoTask(void);

/* @fn uint8_t processMeteoNMEA(uint8_t rc);
 * @brief Process NMEA data received from broadcast
 * @param[in] rc data received
 * @return 1 If reception is complete, 0 if not
 */
uint8_t processMeteoNMEA(uint8_t rc);

/* @fn uint8_t calcNMEAChecksum(char *sentence);
 * @brief Calculates the NMEA sentence checksum
 * @param[in] sentence NMEA sentence
 * @return nmea checksum
 */
uint8_t calcNMEAChecksum(char *sentence);

/* @fn void DelayMS(uint32_t milisec);
 * @brief Produces a delay of indicated milliseconds
 * @param[in] milisec Milleconds to wait
 */
void DelayMS(uint32_t milisec);

/* @fn void ByteRcvFromBroadcast(uint8_t rc);
 * @brief Receive a byte prom broadcast and process it
 * @param[in] rc byte received
 */
void ByteRcvFromBroadcast(uint8_t rc);


/* @fn uint8_t BuffBroadcast(uint8_t *buff, uint16_t bufLen){
 * @brief Broadcast a given buffer
 * @param[in] buff buffer to process
 * @param[in] bufLen buffer length
 * @return weather the buffer can be sent or not.
 */

uint8_t BuffBroadcast(uint8_t *buff, uint16_t bufLen){
	static uint8_t busy = 0;
	static uint8_t tail = 0;
	static uint8_t head = 0;
	static SENDBUF cola[SENDBUF_SIZE];

	cola[head].pBuff = buff;
	cola[head].bufLen = bufLen;
	head = (head+1) % SENDBUF_SIZE;

	if (tail == head)	// Buffer sobreescrito
		tail = (tail+1) % SENDBUF_SIZE;

	if (busy){
		return XBEE_SUCCESS;
	}
	busy = 1;
	LedToggle(LED_3);
	do {
		if (XBeeSendBuffer(cola[tail].pBuff,cola[tail].bufLen) == XBEE_NO_INIT){
			tail = head = 0;
			LedOff(LED_3);
			busy = 0;
			return XBEE_NO_INIT;
		}
		tail = (tail+1) % SENDBUF_SIZE;
	}while(tail != head);
	busy = 0;
	return XBEE_SUCCESS;
}



void timerFunc(void){
	return;
}

int main (void){
 	timer_config timerInit = {TIMER_C,1000,timerFunc};
 	METEO_CONFIG meteoConfig = {0.6320,0.0203,0.500,0.01,1.0546,0.00216,3.3,1023.0};

#if BASE_MODE
 	UBLOX_BASE_STRUCT ubloxBase = {60,100000,BuffBroadcast};
#endif

	SystemClockInit();

	TimerInit(&timerInit);
	LedsInit();
	TimerStart(TIMER_C);

	ConsoleInit();

	ConsoleCLS();

	DelayMS(1000);	// Espera que se energicen los modulos
	consoleSendString("--- RTK Broadcast Project ---");
	consoleSendString("\nInicializando Meteo... ");
	MeteoInit(meteoConfig);
	consoleSendString(" OK");

#if BASE_MODE
	consoleSendString("\nInicializando Ublox... ");
	UBloxBaseInit(ubloxBase);
	consoleSendString(" OK");
	consoleSendString("\nInicializando XBee... ");
	xbeeInit(0);
	consoleSendString(" OK");
#elif ROVER_MODE
	consoleSendString("\nInicializando Ublox... ");
	UBloxRoverInit();
	consoleSendString(" OK");
	consoleSendString("\nInicializando XBee... ");
	xbeeInit(ByteRcvFromBroadcast);
	consoleSendString(" OK");
#endif
	DelayMS(500);

//	 Bucle principal
	ConsoleCLS();
	while(1){
		ConsoleTask();
		MeteoTask();
	}

	return 0;

}



void ConsoleTask(void){
	static uint32_t timestamp = 0;
	static uint8_t clrCnt = 0;
	if (TickDiff(timestamp) >= 0.5 * TicksPerSecond()){
		clrCnt++;
		if (clrCnt == 20){
			ConsoleCLS();
			clrCnt = 0;
		}
		else
			ConsoleResetCursor();

		timestamp = TickGet();

		UBLOX_RTCM_STATE *rtcmState = UBloxGetRTCMSentences();
		UBLOX_GEO *geoData = UBloxGetGeoData();
		METEO_DATA *meteoData = MeteoGetData();

		UBLOX_NAV_SVIN_SATE *svinState = UBloxGetSurveyStatus();




		consoleSendString("--- BASE RTK ---");
		consoleSendString("\nDatos Meteorologicos: ");
		consoleSendString("\n - Temperatura: ");
		consoleSendString(UartItoa(meteoData->temp, 10));
		consoleSendString("°C");
		consoleSendString("\n - Humedad: ");
		consoleSendString(UartItoa(meteoData->hum, 10));
		consoleSendString("%");

		consoleSendString("\nGeoposicionamiento: ");
		consoleSendString("\n - Latitud: ");
		consoleSendString(UartItoa(geoData->latDeg, 10));
		consoleSendString("° ");
		consoleSendString(UartItoa(geoData->latMin, 10));
		consoleSendString(".");
		consoleSendString(UartItoa(geoData->latMinDec, 10));
		consoleSendString("' ");
		consoleSendChar(geoData->latDir);

		consoleSendString("\n - Longitud: ");
		consoleSendString(UartItoa(geoData->lonDeg, 10));
		consoleSendString("° ");
		consoleSendString(UartItoa(geoData->lonMin, 10));
		consoleSendString(".");
		consoleSendString(UartItoa(geoData->lonMinDec, 10));
		consoleSendString("' ");
		consoleSendChar(geoData->lonDir);

		consoleSendString("\n - Hora: ");
		consoleSendString(UartItoa(geoData->hour, 10));
		consoleSendString(":");
		consoleSendString(UartItoa(geoData->min, 10));
		consoleSendString(":");
		consoleSendString(UartItoa(geoData->sec, 10));

		consoleSendString("\n - Valido: ");
		consoleSendString(geoData->valid ? "SI":"NO");

#if BASE_MODE
		consoleSendString("\nSentencias RTCM recibidas del UBlox: ");
#elif ROVER_MODE
		consoleSendString("\nSentencias RTCM recibidas por broadcast: ");
#endif

		consoleSendString("\n - 1005: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1005, 10));
		consoleSendString("\n - 1074: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1074, 10));
		consoleSendString("\n - 1084: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1084, 10));
		consoleSendString("\n - 1094: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1094, 10));
		consoleSendString("\n - 1124: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1124, 10));
		consoleSendString("\n - 1230: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1230, 10));
		consoleSendString("\n - 4072: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM4072, 10));
		consoleSendString("\n - CRC Errors: ");
		consoleSendString(UartItoa(rtcmState->crcError, 10));
#if BASE_MODE
		consoleSendString("\nSurvey-In:");
		consoleSendString("\n - Status: ");
		consoleSendString(svinState->active ? "ACTIVO" : "NO ACTIVO");
		consoleSendString(" | ");
		consoleSendString(svinState->valid ? "VALIDO" : "NO VALIDO");
		consoleSendString("\n - Duracion: ");
		consoleSendString(UartItoa(svinState->dur, 10));
		consoleSendString("\n - Precision: ");
		consoleSendString(UartItoa(svinState->meanAcc/10000, 10));
		consoleSendString(".");
		consoleSendString(UartItoa(svinState->meanAcc%10000, 10));
		consoleSendString(" mts");
#elif ROVER_MODE
		UBLOX_ROVER_MSGS *roverMsgs = UBloxGetRoverMsgs();
		UBLOX_UBX_STATUS *roverStatus = UBloxGetRoverStatus();

		onsoleSendString("\nRover Messeges: ");
//		consoleSendString("\n - Signal: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_SIG, 10));
//		consoleSendString("\n - Status: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_STATUS, 10));
		consoleSendString("\n - Status: ");
		consoleSendString(UartItoa(roverMsgs->NAV_STATUS, 10));

		consoleSendString("\n - Correccion diferencial: ");
		consoleSendString(roverStatus->diffCorr ? "ACTIVA" : "INACTIVA");
		consoleSendString(" | ");
		consoleSendString(roverStatus->diffSoIn ? "CORRIGIENDO" : "NO CORRIGIENDO");
		consoleSendString("\n - Modo: ");
		if (roverStatus->carrSoIn == 0)
			consoleSendString("INACTIVO");
		else if (roverStatus->carrSoIn == 1)
			consoleSendString("RTK Flotante");
		else if (roverStatus->carrSoIn == 1)
			consoleSendString("RTK Fijo");

//		consoleSendString("\n - Rel Pos: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_RELPOSNED, 10));
//		consoleSendString("\n - PosLLH: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_POSLLH, 10));
//		consoleSendString("\n - PosVelTime: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_PVT, 10));
		consoleSendString("\n - RTCM Sentences: ");
		consoleSendString(UartItoa(roverMsgs->RXM_RTCM, 10));
#endif
	}
}
#if 0
#elif ROVER_MODE


void ConsoleTask(void){
	static uint32_t timestamp = 0;
	static uint8_t clrCnt = 0;
	if (TickDiff(timestamp) >= 0.5 * TicksPerSecond()){
		clrCnt++;
		if (clrCnt == 20){
			ConsoleCLS();
			clrCnt = 0;
		}
		else
			ConsoleResetCursor();

		timestamp = TickGet();

		UBLOX_ROVER_MSGS *roverMsgs = UBloxGetRoverMsgs();
		UBLOX_GEO *geoData = UBloxGetGeoData();
		UBLOX_RTCM_STATE *rtcmState = UBloxGetRTCMSentences();
		METEO_DATA *meteoData = MeteoGetData();
		UBLOX_UBX_STATUS *roverStatus = UBloxGetRoverStatus();

		consoleSendString("--- ROVER RTK ---");
		consoleSendString("\nDatos Meteorologicos: ");
		consoleSendString("\n - Temperatura: ");
		consoleSendString(UartItoa(meteoData->temp, 10));
		consoleSendString("°C");
		consoleSendString("\n - Humedad: ");
		consoleSendString(UartItoa(meteoData->hum, 10));
		consoleSendString("%");

		consoleSendString("\nGeoposicionamiento: ");
		consoleSendString("\n - Latitud: ");
		consoleSendString(UartItoa(geoData->latDeg, 10));
		consoleSendString("° ");
		consoleSendString(UartItoa(geoData->latMin, 10));
		consoleSendString(".");
		consoleSendString(UartItoa(geoData->latMinDec, 10));
		consoleSendString("' ");
		consoleSendChar(geoData->latDir);
		consoleSendString("\n - Longitud: ");
		consoleSendString(UartItoa(geoData->lonDeg, 10));
		consoleSendString("° ");
		consoleSendString(UartItoa(geoData->lonMin, 10));
		consoleSendString(".");
		consoleSendString(UartItoa(geoData->lonMinDec, 10));
		consoleSendString("' ");
		consoleSendChar(geoData->lonDir);
		consoleSendString("\n - Hora: ");
		consoleSendString(UartItoa(geoData->hour, 10));
		consoleSendString(":");
		consoleSendString(UartItoa(geoData->min, 10));
		consoleSendString(":");
		consoleSendString(UartItoa(geoData->sec, 10));

		consoleSendString("\n - Valido: ");
		consoleSendString(geoData->valid ? "SI":"NO");


		consoleSendString("\nSentencias RTCM recibidas por broadcast: ");
		consoleSendString("\n - 1005: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1005, 10));
		consoleSendString("\n - 1074: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1074, 10));
		consoleSendString("\n - 1084: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1084, 10));
		consoleSendString("\n - 1094: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1094, 10));
		consoleSendString("\n - 1124: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1124, 10));
		consoleSendString("\n - 1230: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM1230, 10));
		consoleSendString("\n - 4072: ");
		consoleSendString(UartItoa(rtcmState->cntRTCM4072, 10));
		consoleSendString("\n - CRC Errors: ");
		consoleSendString(UartItoa(rtcmState->crcError, 10));


		consoleSendString("\nRover Messeges: ");
//		consoleSendString("\n - Signal: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_SIG, 10));
//		consoleSendString("\n - Status: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_STATUS, 10));
		consoleSendString("\n - Status: ");
		consoleSendString(UartItoa(roverMsgs->NAV_STATUS, 10));

		consoleSendString("\n - Correccion diferencial: ");
		consoleSendString(roverStatus->diffCorr ? "ACTIVA" : "INACTIVA");
		consoleSendString(" | ");
		consoleSendString(roverStatus->diffSoIn ? "CORRIGIENDO" : "NO CORRIGIENDO");
		consoleSendString("\n - Modo: ");
		if (roverStatus->carrSoIn == 0)
			consoleSendString("INACTIVO");
		else if (roverStatus->carrSoIn == 1)
			consoleSendString("RTK Flotante");
		else if (roverStatus->carrSoIn == 1)
			consoleSendString("RTK Fijo");

//		consoleSendString("\n - Rel Pos: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_RELPOSNED, 10));
//		consoleSendString("\n - PosLLH: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_POSLLH, 10));
//		consoleSendString("\n - PosVelTime: ");
//		consoleSendString(UartItoa(roverMsgs->NAV_PVT, 10));
		consoleSendString("\n - RTCM Sentences: ");
		consoleSendString(UartItoa(roverMsgs->RXM_RTCM, 10));
	}
}
#endif



void MeteoTask(void){
#if BASE_MODE
	static uint32_t updateTimestamp = 0;
	static uint32_t sendTimestamp = 0;


	if (TickDiff(updateTimestamp) >= 0.01 * TicksPerSecond()){
		updateTimestamp = TickGet();
		MeteoUpdate();
	}

	if (TickDiff(sendTimestamp) >= TicksPerSecond()){
		sendTimestamp = TickGet();
		METEO_DATA *meteoData = MeteoGetData();
		uint8_t chk;
//		char txt[64] = "$ECTHU,TEMP,HUM*CC\r\n";
		char txt[64] = "$ECTHU,";
		char *aux;

		aux = UartItoa(meteoData->temp, 10);
		strcat(txt,aux);
		strcat(txt,",");
		aux = UartItoa(meteoData->hum, 10);
		strcat(txt,aux);


		chk = calcNMEAChecksum(txt+1);

		strcat(txt,"*");
		aux = UartItoa(chk, 16);
		strcat(txt,aux);
		strcat(txt,"\r\n");

		BuffBroadcast((uint8_t *) txt, strlen(txt));
	}

#endif

}

void ByteRcvFromBroadcast(uint8_t rc){
	static uint8_t receiving = 0; // 0 - No | 1 - RTK | 2 - METEO
//	static uint8_t buff[1024];
//	static uint16_t buffcnt = 0;

	if (!receiving){
//		buffcnt = 0;

		if (rc == 0xD3){
			receiving = 1;
			LedOn(LED_1);
		}
		else if (rc == '$'){
			receiving = 2;
			LedOn(LED_2);
		}

	}

//	buff[buffcnt] = rc;
//	buffcnt++;

	if (receiving == 1){
		if (UBloxRTCMProcesss(rc)){	// Termino de recibir la trama
			receiving = 0;
			LedOff(LED_1);
		}
	}
	else if (receiving == 2){
		if (processMeteoNMEA(rc)){
			receiving = 0;
			LedOff(LED_2);
		}

//		if (UBloxRTCMProcesss(rc))	// Termino de recibir la trama
//			receiving = 0;
	}

}


void DelayMS(uint32_t milisec){
	uint32_t timestamp = TickGet();
	while(TickDiff(timestamp) <  milisec * TicksPerSecond() / 1000);
}

uint8_t calcNMEAChecksum(char *sentence){
	uint8_t chk = 0;
	uint8_t len = 0;
	uint8_t i = 0;

	len = strlen(sentence);
	for (i = 0; i < len; i++){
		chk ^= sentence[i];
	}

	return chk;
}

uint8_t processMeteoNMEA(uint8_t rc){
	static uint8_t nmeaBuff[64];
	static uint8_t nmeaCnt = 0;
//	static uint8_t pending = 0xFFFF;



	if (nmeaCnt == 0 && rc != '$'){
		return 1;	// Deja de procesar, error
	}

	nmeaBuff[nmeaCnt] = rc;
	nmeaCnt++;

	if (nmeaCnt >= 64){	// overflow
		nmeaCnt = 0;
		return 1;
	}


	if (rc == '\n'){
		char *tok;
		uint8_t sentenceChk;
		uint8_t rcvChk;
		nmeaBuff[nmeaCnt] = '\0';	// Por las dudas, temina en 0

		tok = strtok((char *)nmeaBuff,"*");
		sentenceChk = calcNMEAChecksum(tok+1);

		tok = strtok(0,"\n");
		rcvChk = strtol((char *) tok,0,16);
		if (rcvChk == sentenceChk){
			// Checksum ok

			tok = strtok((char*)nmeaBuff,",");
			if (!strcmp(tok,"$ECTHU")){	// Son iguales, ok!
				uint8_t temp;
				uint8_t hum;
				tok = strtok(0,",");	// saco la temperatura
				if (!tok){
					nmeaCnt = 0;
					return 1;	// Error!!!!
				}
				temp = atoi(tok);
				tok = strtok(0,",");	// saco la humedad
				if (!tok){
					nmeaCnt = 0;
					return 1;	// Error!!!!
				}
				hum = atoi(tok);
				MeteoSetData(temp, hum);
			}
		}

		nmeaCnt = 0;
		return 1;	// Termino de pŕocesar


	}

	return 0;	// Seguir pŕocesando
}
