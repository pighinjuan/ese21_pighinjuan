/*
 * electrocardiograma.c
 *
 *  Created on: 13 sep. 2021
 *      Author: juany
 */

#include "electrocardiograma.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"
#include "led.h"
#include "analog_io.h"
#include <string.h>


const uint8_t ECG1[]={
		76, 77, 78, 77, 79, 86, 81, 76, 84, 93, 85, 80,
		89, 95, 89, 85, 93, 98, 94, 88, 98, 105, 96, 91,
		99, 105, 101, 96, 102, 106, 101, 96, 100, 107, 101,
		94, 100, 104, 100, 91, 99, 103, 98, 91, 96, 105, 95,
		88, 95, 100, 94, 85, 93, 99, 92, 84, 91, 96, 87, 80,
		83, 92, 86, 78, 84, 89, 79, 73, 81, 83, 78, 70, 80, 82,
		79, 69, 80, 82, 81, 70, 75, 81, 77, 74, 79, 83, 82, 72,
		80, 87, 79, 76, 85, 95, 87, 81, 88, 93, 88, 84, 87, 94,
		86, 82, 85, 94, 85, 82, 85, 95, 86, 83, 92, 99, 91, 88,
		94, 98, 95, 90, 97, 105, 104, 94, 98, 114, 117, 124, 144,
		180, 210, 236, 253, 227, 171, 99, 49, 34, 29, 43, 69, 89,
		89, 90, 98, 107, 104, 98, 104, 110, 102, 98, 103, 111, 101,
		94, 103, 108, 102, 95, 97, 106, 100, 92, 101, 103, 100, 94, 98,
		103, 96, 90, 98, 103, 97, 90, 99, 104, 95, 90, 99, 104, 100, 93,
		100, 106, 101, 93, 101, 105, 103, 96, 105, 112, 105, 99, 103, 108,
		99, 96, 102, 106, 99, 90, 92, 100, 87, 80, 82, 88, 77, 69, 75, 79,
		74, 67, 71, 78, 72, 67, 73, 81, 77, 71, 75, 84, 79, 77, 77, 76, 76,
};

const uint8_t ECG2[] = {
		17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
		18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
		10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
		38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
		133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
		24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
		17,17,17
};

uint16_t adcVal = 0;

void timerFunc(void);


void uBloxReceiveData(void);

void adcFunc(void);


void timerFunc(void){
	static uint8_t ecCount = 0;
	char txt[32];


	uint8_t val = (uint8_t) (1.0 * ECG2[ecCount] * adcVal / ANALOG_OUTPUT_RANGE);
	LedToggle(1);


//	txt = UartItoa(, 10);
	if (val > 0)
		strcpy(txt,(char *)UartItoa(val, 10));
	else
		strcpy(txt,"0");
	strcat(txt,"\n");
	UartSendString(SERIAL_PORT_PC, (unsigned char *) txt);
	ecCount++;
	if (!(ecCount%sizeof(ECG2)))
		ecCount = 0;

}

void uBloxReceiveData(void){

	uint8_t readVal;
	if (UartReadByte(SERIAL_PORT_PC,&readVal))
		LedToggle(2);
}


void adcFunc(void){
	uint16_t value;
	AnalogInputRead(CH1, &value);
	adcVal = (uint16_t) (adcVal*0.9 + 0.1 * value);
	LedToggle(4);
	AnalogStartConvertion();
}

int main (void){
	// Inicializaciones
	timer_config timerInit = {TIMER_C,4000,timerFunc};
	SERIAL_PORT serialinit = {SERIAL_PORT_PC,115200,uBloxReceiveData};
	analog_input_config adcInit = {CH1,AINPUTS_SINGLE_READ,adcFunc};


	SystemClockInit();
	TimerInit(&timerInit);
	UartInit(&serialinit);
	LedsInit();
	AnalogInputInit(&adcInit);
//
//
	TimerStart(TIMER_C);
	AnalogStartConvertion();

	// Bucle principal
	while(1);

	return 0;

}
