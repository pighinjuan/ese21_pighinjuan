/*
 * electrocardiograma.h
 *
 *  Created on: 13 sep. 2021
 *      Author: juany
 */

#ifndef ELECTROCARDIOGRAMA_H_
#define ELECTROCARDIOGRAMA_H_


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/



#endif /* ELECTROCARDIOGRAMA_H_ */
