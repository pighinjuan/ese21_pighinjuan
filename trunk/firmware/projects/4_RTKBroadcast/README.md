## Comunicación de sentencias RTK para correccion de sistema GPS
### Objetivo
El sistema debe ser capaz de recibir sentencias RTK de un modulo GPS y transmitirlas en modo broadcast por radiofrecuencia de largo alcance. El mismo software debe ser capaz de recibir las sentencias desde el modulo RF y reenviarselas al modulo GPS para poder realizar la correccion de posición. Se agrega el sensado de variables meteorologicas de temperatura y humedad con sensores analógicos para poder transmitirlas de un dispositivo al otro
### Antecedentes
Se cuenta con un sistema de monitoreo de maquinaria agricola en el que se requiere aumentar la precisión de GPS. Para esto se decide agregar una base que transmita sentencias RTK en modo broadcast utilizando un modulo RF. El driver debe ser funcional tanto para la base como para la el dispositivo de monitoreo. Cada dispositivo de monitoreo cuenta con una estación meteorológica que podria omitirse si los datos se reciben de una central meteorológica central.
### Diagrama de bloques

![alt text](blocks.png "Title Text")

### Modulos empleados
* Para la comunicación RF se utliza el módulo XBee-PRO 900HP
* El módulo GPS utilizado es U-Blox ZED-F9P
* Sensor de humedad analógico lineal HIH 4031
* Sensor de temperatura analógico lineal TC 1047A
