/*
 * newLibImpl.c
 *
 *  Created on: 23 oct. 2021
 *      Author: juany
 */




#include <errno.h>
#include <sys/stat.h>
//#include "utils.h"
//#include <stdio.h>
#include <unistd.h>
#undef errno

extern int errno ;

extern char * _pvHeapStart;
extern char * __top_RamLoc40 ;

void * _sbrk(int incr){

	char * prev_heap_end;
	static char *lastHeapAssigned = (char *)&_pvHeapStart;
	prev_heap_end = lastHeapAssigned;
	lastHeapAssigned += incr ;
	if (lastHeapAssigned >= (char *) (&__top_RamLoc40)){
		lastHeapAssigned = prev_heap_end;
		errno = ENOMEM;
		abort();
	}

	return ( void *) prev_heap_end ;
}

void abort (void){
	return;
}

int _close (int file) {
	return -1;
}

int _fstat (int file, struct stat *st) {
	st->st_mode = S_IFCHR;
	return 0;
}

int _isatty (int file) {
	return 1;
}

int _lseek (int file, int offset, int whence) {
	return 0;
}

int _read (int file, char *ptr, int len) {
	return -1;
}

int _write (int file, char *ptr, int len) {
	return -1;

}
