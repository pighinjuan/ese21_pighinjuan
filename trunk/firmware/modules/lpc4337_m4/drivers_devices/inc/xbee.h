/*
 * xbee.h
 *
 *  Created on: 29 sep. 2021
 *      Author: juany
 */

#ifndef _XBEE_H_
#define _XBEE_H_


/** \addtogroup XBee broadcast mode
 ** @{ */

/* \brief Driver to use Xbee as to broadcast or receive broadcast data
 *
 */

/*
 * Initials     Name
 * ---------------------------
 *	JAP			Juan Antonio Pighin
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211031 v0.1 initials initial version JAP
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

/*==================[macros]=================================================*/

#define NO_INIT

#define XBEE_SUCCESS 0
#define XBEE_NO_INIT 1
#define XBEE_BUSY 2
/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/

/* @fn void xbeeInit(void *byteProcessBuff)
 * @brief Initialize XBee module,
 * @param[in] byteProcessBuff Pointer to function to process byte received from broadcast.
 */
void xbeeInit(void *byteProcessBuff);

/* @fn uint8_t XBeeSendBuffer(uint8_t *buf, uint8_t len)
 * @brief Function to broadcast a buffer
 * @param[in] buf Pointer to buffer to broadcast
 * @param[in] len Buffer length
 * @return weather the buffer could be sent or not
 */
uint8_t XBeeSendBuffer(uint8_t *buf, uint8_t len);

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* _XBEE_H_ */
