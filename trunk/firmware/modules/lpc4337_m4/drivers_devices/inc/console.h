/*
 * console.h
 *
 *  Created on: 9 oct. 2021
 *      Author: juany
 */

#ifndef _CONSOLE_H_
#define _CONSOLE_H_

/** \addtogroup Console
 ** @{ */

/* \brief Driver to interact with serial console
 *
 */

/*
 * Initials     Name
 * ---------------------------
 *	JAP			Juan Antonio Pighin
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211031 v0.1 initials initial version JAP
 */

/*==================[inclusions]=============================================*/


/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/* @fn void ConsoleInit(void)
 * @brief Console Initializacion
 * Initialize he UART for console comunicacion
 */
void ConsoleInit(void);
/* @fn void consoleSendString(char *txt)
 *
 * @brief Sends a string using the console UART
 *
 * @param[in] txt Null ended string
 */
void consoleSendString(char *txt);

/* @fn void consoleSendChar(char ch)
 *
 * @brief Sends a char using the console UART
 *
 * @param[in] ch Character to send
 */
void consoleSendChar(char ch);

/* @fn void ConsoleCLS(void)
 *
 * @brief Sends a command to clean the screen and reset cursor position
 *
 */
void ConsoleCLS(void);

/* @fn void ConsoleCLS(void)
 *
 * @brief Sends a command to reset cursor position
 *
 */
void ConsoleResetCursor(void);

#endif /* _CONSOLE_H_ */
