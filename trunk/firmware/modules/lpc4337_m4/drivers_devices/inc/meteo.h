/*
 * meteo.h
 *
 *  Created on: 26 oct. 2021
 *      Author: juany
 */

#ifndef _METEO_H_
#define _METEO_H_


/** \addtogroup MeteorologicalADC
 ** @{ */

/* \brief Driver to get Meteorological data (Temperature and Humidity) from linear analog devices.
 *
 * - Configurable ADC range.
 * - Allows humidity correction with temperature.
 * - Uses analog driver for EDU-CIAA
 * - Allows external set for meteorological data
 */

/*
 * Initials     Name
 * ---------------------------
 *	JAP			Juan Antonio Pighin
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20211031 v0.1 initials initial version JAP
 */

/*==================[inclusions]=============================================*/
#include <stdint.h>

/*==================[typedef]================================================*/
typedef struct{				//!< Meteo data Struct
	uint16_t hum;			//!< Humidity
	uint16_t temp;			//!< Temperature
}METEO_DATA;

typedef struct{					//!< Meteo config struct
	float humOffset;			//!< Humidity Offset [%]
	float humSlope;				//!< Humidity Slope [v/%]
	float tempOffset;			//!< Temperature Offset [°C]
	float tempSlope;			//!< Temperature Slope [v/°C]
	float tempCorrHumOffset;	//!< Temperature correction humidity Offset [no units]
	float tempCorrHumSlope;		//!< Temperature correction humidity Slope [1/°C]
	float vIn;					//!< Reference voltage for ADC
	float adcMax;				//!< Max ADC configuration
}METEO_CONFIG;

/*==================[external data declaration]==============================*/


/*==================[external functions declaration]=========================*/
/** @fn void MeteoInit(METEO_CONFIG meteoConfig)
 * @brief Module initialization.
 * @param[in] meteoConfig Initilization parameters struct: affstes and slopes for linear convertions
 * @return null
 */
void MeteoInit(METEO_CONFIG meteoConfig);
/* @fn void MeteoUpdate(void)
 * @brief Updates the Temp-Hum values based on adc reads
 * @return null
 */
void MeteoUpdate(void);

/* @fn METEO_DATA *MeteoGetData(void);
 * @brief Get current meteorological data values
 * @return METEO_DATA struct with meteorological data
 */
METEO_DATA *MeteoGetData(void);

/* @fn void MeteoSetData(uint16_t temperatura, uint16_t humedad);
 * @brief External set for meteorological data
 * @return null
 */
void MeteoSetData(uint16_t temperatura, uint16_t humedad);

/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* _METEO_H_ */
