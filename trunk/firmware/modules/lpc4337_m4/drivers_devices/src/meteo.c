/*
 * meteo.c
 *
 *  Created on: 26 oct. 2021
 *      Author: juany
 */




#include "meteo.h"
#include "led.h"
#include "analog_io.h"
#include <string.h>

METEO_DATA meteoData;

uint16_t adcTemp;
uint16_t adcHum;


METEO_CONFIG meteoConfig;


void MeteoInit(METEO_CONFIG meteoInit){
	// Inicializaciones
	analog_input_config adcInit = {CH2 | CH3,AINPUTS_BURST_READ,0};

	adcTemp = 0;
	adcHum = 0;

	memcpy(&meteoConfig,&meteoInit,sizeof(METEO_CONFIG));

	AnalogInputInit(&adcInit);

	meteoData.temp = 0;
	meteoData.hum = 0;

}

void MeteoUpdate(void){
	uint16_t value;
	float temp;
	if (AnalogGetCHStatus(CH3)){
		AnalogInputRead(CH3, &value);
		adcTemp = (uint16_t)(value * 0.05 + 0.95 * adcTemp);
		temp = adcTemp  * meteoConfig.vIn / meteoConfig.adcMax;
		meteoData.temp = (uint16_t) ((temp - meteoConfig.tempOffset ) / meteoConfig.tempSlope);

	}
	if (AnalogGetCHStatus(CH2)){
		AnalogInputRead(CH2, &value);
		adcHum = (uint16_t)(value * 0.05 + 0.95 * adcHum);
		temp = adcHum * meteoConfig.vIn / meteoConfig.adcMax;
		temp  = (uint16_t) ((temp - meteoConfig.humOffset ) / meteoConfig.humSlope);
		meteoData.hum = (uint16_t) (temp / (meteoConfig.tempCorrHumOffset - meteoConfig.tempCorrHumSlope * meteoData.temp));
	}
}


METEO_DATA *MeteoGetData(void){
	return &meteoData;
}

void MeteoSetData(uint16_t temperatura, uint16_t humedad){
	meteoData.temp = temperatura;
	meteoData.hum = humedad;
}

