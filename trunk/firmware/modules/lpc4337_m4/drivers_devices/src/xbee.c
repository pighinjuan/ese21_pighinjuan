/*
 * xbee.c
 *
 *  Created on: 29 sep. 2021
 *      Author: juany
 */
/*==================[inclusions]=============================================*/
#include "xbee.h"
#include "uart.h"
#include <stdint.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
#define XBEE_SERIAL_PORT SERIAL_PORT_GPIO_1_2
#define BUFFLEN 1024



/*==================[internal functions declaration]=========================*/
/* @fn void xbeeBroadcastConfig(void)
 * @brief Configures the modute to bradcast
 */
void xbeeBroadcastConfig(void);

/* @fn void xbeeProcessByte(uint8_t rc)
 * @brief Verifies the buffer received
 * @param[in] rc Bute received
 */
void xbeeProcessByte(uint8_t rc);

/* @fn void xbeeReceiveData(void)
 * @brief Process the byte received from Uart
 */
void xbeeReceiveData(void);

/*==================[external data declaration]==============================*/


/*==================[internal data declaration]=========================*/
static RINGBUFF_T rbTx, rbRx;
static uint8_t txBuff[BUFFLEN], rxBuff[BUFFLEN];

static  SERIAL_PORT_RB serialPort = {{XBEE_SERIAL_PORT,9600,xbeeReceiveData},
										&rbRx,
										rxBuff,
										BUFFLEN,
										&rbTx,
										txBuff,
										BUFFLEN};	//!< Serial Port RB config
uint8_t waitForOk = 0;			//!< Flag for the data received
uint8_t rcvBuff[64];			//!< Buffer for xbee responses
uint8_t rcvCnt;					//!< Buffer use
uint8_t xbeeInitialized = 0;	//!< Xbee initialized flag
uint8_t (*XBeeByteReceived)(uint8_t rc) = NO_INT;	//!< Pointer to funtion to send the bytes received

/*==========================[typedefs]==========================*/


void xbeeReceiveData(void){
	uint8_t readVal;
	UART_IRQRBHandler(serialPort);

	if (UartReadByteRB(serialPort,&readVal)){

		if (waitForOk)
			xbeeProcessByte(readVal);
		else if(XBeeByteReceived){
			XBeeByteReceived(readVal);
		}
	}
}

void xbeeInit(void *byteProcessBuff){
	xbeeInitialized = 0;

	UartInitRB(&serialPort);

	rcvCnt = 0;

	XBeeByteReceived = byteProcessBuff;

	xbeeBroadcastConfig();

	xbeeInitialized = 1;
}



void xbeeBroadcastConfig(void){
	UartSendStringRB(serialPort, "+++");
	waitForOk = 1;
	while(waitForOk);

	UartSendString(serialPort.serialPort, "ATDTFFFF\r");
	waitForOk = 1;
	while(waitForOk);

	UartSendString(serialPort.serialPort, "ATRB20\r");
	waitForOk = 1;
	while(waitForOk);

	UartSendString(serialPort.serialPort, "ATRO32\r");
	waitForOk = 1;
	while(waitForOk);

	UartSendString(serialPort.serialPort, "ATPK64\r");
	waitForOk = 1;
	while(waitForOk);

	UartSendString(serialPort.serialPort, "ATWR\r");
	waitForOk = 1;
	while(waitForOk);

	UartSendString(serialPort.serialPort, "ATCN\r");
	waitForOk = 1;
	while(waitForOk);

}


void xbeeProcessByte(uint8_t rc){

	rcvBuff[rcvCnt] = rc;
	rcvCnt++;

	if (rc == '\r' || rc == '\n'){
		rcvBuff[rcvCnt-1] = '\0';
		if (!strcmp((char *) rcvBuff,"OK")){
			waitForOk = 0;
			rcvCnt = 0;
		}
		else if (!strcmp((char *) rcvBuff,"ERROR")){
			waitForOk = 0;
			rcvCnt = 0;
		}
	}
}

uint8_t XBeeSendBuffer(uint8_t *buf, uint8_t len){
	static uint8_t lock = 0;
	if (lock)
		return XBEE_BUSY;
	lock = 1;
	if (xbeeInitialized){
		if (UartSendBufferRB(serialPort, buf, len) != len){
		}

		lock = 0;
		return XBEE_SUCCESS;
	}
	lock = 0;
	return XBEE_NO_INIT;
}
