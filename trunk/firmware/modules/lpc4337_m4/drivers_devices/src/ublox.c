/*
 * ublox.c
 *
 *  Created on: 29 sep. 2021
 *      Author: juany
 */
/*==================[inclusions]=============================================*/
#include "ublox.h"
#include "uart.h"
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


/*==================[macros and definitions]=================================*/
#define UBLOX_SERIAL_PORT SERIAL_PORT_P2_CONNECTOR
#define RTCM_HEAD_LEN 3
#define RTCM_CHK_LEN 3

/*==================[internal functions declaration]=========================*/
/* @fn void ubloxInit(void)
 * @brief Generic initialization for the module
 */
void ubloxInit(void);

/* @fn void uBloxProcessData(uint8_t rc)
 * @brief Process data received from UART
 * @parameter[in] rc byte received
 */
void uBloxProcessData(uint8_t rc);

/* @fn void uBloxProcessNMEA(void)
 * @brief Process NMEA data
 */
void uBloxProcessNMEA(void);

/* @fn void uBloxProcessUBX(void)
 * @brief Process UBX data
 */
void uBloxProcessUBX(void);

/* @fn void uBloxProcessRTCM(void)
 * @brief Process RTCM data
 */
void uBloxProcessRTCM(void);

/* @fn void uBloxReceiveData(void)
 * @brief Receive data from interrupt
 */
void uBloxReceiveData(void);

/* @fn void uBloxEnableUart1Msg(uint16_t MsgType, uint8_t period)
 * @brief Used to enable UART messages
 * @param[in] MsgType message type to enable
 * @param[in] periode output frequency
 */
void uBloxEnableUart1Msg(uint16_t MsgType, uint8_t period);

/* @fn void uBloxValSet(uint32_t code, uint32_t value,uint8_t saveMode);
 * @brief Set the value for configuration for the UBlox Module
 * @param[in] code code to change
 * @param[in] value value to assign
 * @param[in] saveMode save mode
 */
void uBloxValSet(uint32_t code, uint32_t value,uint8_t saveMode);


/*==================[internal data declaration]=========================*/
uint32_t crc24q(const uint8_t *buf, uint32_t len, uint32_t crc) ;

uint8_t headBuf[16];		//!< Stores the header of the current sentece
uint8_t bodyBuf[1024];		//!< Stores the body of the current sentence
uint8_t chkBuf[64];			//!< Stores the checksum of the current sentence

UBLOX_RTCM_STATE rtcmState;			//!< Counts RTCM sentences received
UBLOX_NAV_SVIN_SATE navSvin;		//!< Survey-In Status
UBLOX_RXM_RTCM rxmRtcm;				//!< State of RTCM received
UBLOX_ROVER_MSGS roverMsgs;			//!< Rover received messages count
UBLOX_GEO uBloxGeoData;				//!< UBlox geographical data
UBLOX_UBX_STATUS uBloxRoverStatus;	//!< Rover status

uint8_t (*RTKBroadcast)(uint8_t *buf, uint8_t len)= NO_INT;		//!< Pointer to broadcast the RTK senteces
static  SERIAL_PORT serialPort = {UBLOX_SERIAL_PORT,38400,uBloxReceiveData};	//!< Serial port config

enum {
	UBLOX_WAITING_SOF,				//!< Waiting a new frame
	UBLOX_NMEA_READING_HEADER,		//!< Reading NMEA header
	UBLOX_NMEA_READING_BODY,		//!< Reading NMEA body
	UBLOX_NMEA_READING_CHECKSUM,	//!< Reading NMEA checksum
	UBLOX_RTCM_READING_HEADER,		//!< Reading RTCM header
	UBLOX_RTCM_READING_BODY,		//!< Reading RTCM body
	UBLOX_RTCM_READING_CHECKSUM,	//!< Reading RTCM checksum
	UBLOX_UBX_READING_HEADER,		//!< Reading UBX header
	UBLOX_UBX_READING_BODY,			//!< Reading UBX body
	UBLOX_UBX_READING_CHECKSUM,		//!< Reading UBX checksum
//	UBLOX_FAIL
}uBloxReadState;					//!< State machine state

/*==========================[typedefs]==========================*/
typedef struct __attribute__((packed)){
	uint16_t preamble;
	uint8_t class;
	uint8_t ID;
	uint8_t len;
}UBX_HEAD_STRUCT;



void uBloxReceiveData(void){
	uint8_t readVal;
	if (UartReadByte(serialPort,&readVal)){
		uBloxProcessData(readVal);
	}
}


void UBloxBaseInit(UBLOX_BASE_STRUCT ubloxBase){

	ubloxInit();


	uBloxValSet(0x20030001,1,1);	// TMODE: Survey-In Mode
	uBloxValSet(0x40030010,ubloxBase.surveyInTime,1);	// TMODE: Survey-In Min DUration 60 seg
	uBloxValSet(0x40030011,ubloxBase.surveyInPrec,1);	// TMODE: Survey-In Precision  10 mts

	uBloxValSet(0x20910089,1,1);	// Survey-In: Monitor Msj

	uBloxEnableUart1Msg(1005,1);
	uBloxEnableUart1Msg(1074,1);
	uBloxEnableUart1Msg(1084,1);
	uBloxEnableUart1Msg(1094,1);
	uBloxEnableUart1Msg(1124,1);
	uBloxEnableUart1Msg(1230,1);
	uBloxEnableUart1Msg(4072,1);



	RTKBroadcast = ubloxBase.pRTKBroadcast;

}

void UBloxRoverInit(void){

	ubloxInit();

//	uBloxValSet(0x20910346,1,1);	// MSGOUT: NAV-SIG
//	uBloxValSet(0x20910007,1,1);	// MSGOUT: NAV-PVT
//	uBloxValSet(0x2091002A,1,1);	// MSGOUT: NAV-POSLLH
//	uBloxValSet(0x2091008E,1,1);	// MSGOUT: NAV-RELPOSNED
	uBloxValSet(0x2091001B,1,1);	// MSGOUT: NAV-STATUS
	uBloxValSet(0x20910269,1,1);	// MSGOUT: RXM-RTCM


	memset(&roverMsgs,0x00,sizeof(roverMsgs));

}


void ubloxInit(void){

	UartInit(&serialPort);

	uBloxValSet(0x209100c0,0,1);	// MSGOUT: GSA disable
	uBloxValSet(0x209100c5,0,1);	// MSGOUT: GSV disable
	uBloxValSet(0x209100bb,0,1);	// MSGOUT: GGA disable
	uBloxValSet(0x209100b1,0,1);	// MSGOUT: VTG disable
	uBloxValSet(0x209100ac,0,1);	// MSGOUT: RMC disable

	memset(&uBloxGeoData,0x00,sizeof(UBLOX_GEO));
	memset(&rtcmState,0x00,sizeof(rtcmState));

	uBloxReadState = UBLOX_WAITING_SOF;
}

void uBloxProcessData(uint8_t rc){
	static uint8_t *bufptr = headBuf;
	static uint16_t bufCnt = 0;

	static uint16_t rtcmLen = 0;
	static uint32_t rtcmCRC = 0;

	static uint16_t ubxLen = 0;
	static uint8_t ubxChkA = 0;
	static uint8_t ubxChkB = 0;

	static uint8_t nmeaChk = 0;

	bufptr[bufCnt] = rc;
	bufCnt++;

	if (headBuf == bufptr && bufCnt > 16){
		uBloxReadState = UBLOX_WAITING_SOF;	// Overflow
	}
	else if (bodyBuf == bufptr && bufCnt > 1024){
		uBloxReadState = UBLOX_WAITING_SOF;	// Overflow
	}
	else if (chkBuf == bufptr && bufCnt > 64){
		uBloxReadState = UBLOX_WAITING_SOF;	// Overflow
	}

	switch (uBloxReadState){
		case UBLOX_WAITING_SOF:
			if (rc == '$'){	// Nueva trama NMEA
				uBloxReadState = UBLOX_NMEA_READING_HEADER;
				nmeaChk = 0;
			}
			else if (rc == 0xD3){	// Nueva trama RTCM
				uBloxReadState = UBLOX_RTCM_READING_HEADER;
				rtcmCRC = 0;
				rtcmCRC = crc24q(&rc, 1, rtcmCRC);
				rtcmLen = 0;
			}
			else if (rc == 0xB5){
				// Posible primer byte de la trama UBX, esperamos al segundo 0x62
			}
			else if (rc == 0x62 && bufCnt == 2 && headBuf[0] == 0xB5) {	// Nueva trama UBX
				uBloxReadState = UBLOX_UBX_READING_HEADER;
				ubxChkA = 0;
				ubxChkB = 0;
				ubxLen = 0;
			}
			else {
				bufptr = headBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_NMEA_READING_HEADER:
			nmeaChk ^= rc;
			if (rc == ','){
				uBloxReadState = UBLOX_NMEA_READING_BODY;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = bodyBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_NMEA_READING_BODY:
			if (rc == '*'){
				uBloxReadState = UBLOX_NMEA_READING_CHECKSUM;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = chkBuf;
				bufCnt = 0;
			}
			else
				nmeaChk ^= rc;
			break;
		case UBLOX_NMEA_READING_CHECKSUM:
			if (bufCnt == 2){	// 2 Bytes de checksum
				uBloxReadState = UBLOX_WAITING_SOF;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = headBuf;
				bufCnt = 0;
				if (strtol((char *) chkBuf,0,16) == nmeaChk)
					uBloxProcessNMEA();
			}
			break;
		case UBLOX_RTCM_READING_HEADER:
			rtcmCRC = crc24q(&rc, 1, rtcmCRC);
			if (bufCnt == RTCM_HEAD_LEN){	// 3 Byte de header
				uBloxReadState = UBLOX_RTCM_READING_BODY;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				rtcmLen = (headBuf[1] & 0x03) * 256 + headBuf[2];
				bufptr = bodyBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_RTCM_READING_BODY:
			rtcmCRC = crc24q(&rc, 1, rtcmCRC);
			if (bufCnt == rtcmLen){	// La longitud la da el header.
				uBloxReadState = UBLOX_RTCM_READING_CHECKSUM;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = chkBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_RTCM_READING_CHECKSUM:
			if (bufCnt == RTCM_CHK_LEN){	// 3 bytes de CRC
				uint32_t crc;
				uBloxReadState = UBLOX_WAITING_SOF;
				crc = (((((uint32_t) chkBuf[0]) * 256 ) + chkBuf[1]) * 256 ) + chkBuf[2];
				if (crc == rtcmCRC){
					uBloxProcessRTCM();
				}
				else{
					rtcmState.crcError++;
				}
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = headBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_UBX_READING_HEADER:
			ubxChkA = ubxChkA + rc;
			ubxChkB = ubxChkB + ubxChkA;
			if (bufCnt == 6){
				uBloxReadState = UBLOX_UBX_READING_BODY;
				ubxLen = (headBuf[5]) * 256 + headBuf[4];
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = bodyBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_UBX_READING_BODY:
			ubxChkA = ubxChkA + rc;
			ubxChkB = ubxChkB + ubxChkA;
			if (bufCnt == ubxLen){
				uBloxReadState = UBLOX_UBX_READING_CHECKSUM;
				bufptr[bufCnt] = '\0';	// Termina en \0 para el saber hasta donde procesar
				bufptr = chkBuf;
				bufCnt = 0;
			}
			break;
		case UBLOX_UBX_READING_CHECKSUM:
			if (bufCnt == 2){
				uBloxReadState = UBLOX_WAITING_SOF;
				if (chkBuf[0] == ubxChkA && chkBuf[1] == ubxChkB ){
					uBloxProcessUBX();
				}
				bufptr = headBuf;
				bufCnt = 0;
			}
			break;
	}
}

void uBloxProcessNMEA(void) {
	if (!strcmp((char *) headBuf,"$GNGLL,")){
		char aux[16];
		char *ptr;
		int i;
		char *tok = strtok((char *)bodyBuf,"*");
		int dataIdx = 0;
		uint32_t hora;
		tok = strtok((char *)bodyBuf,",");
		if (bodyBuf[0] == ','){
			if (*tok == 'V'){
				uBloxGeoData.valid = 0;
				return;
			}
			dataIdx = 4;	// Salta directo a la hora porque no hay otros datos
		}
		while (tok){
			switch (dataIdx){
				case 0:	// Latitud
					if (*ptr == ',')
						break;
					ptr = tok;
					i=0;
					while(*ptr != '.'){
						aux[i] = *ptr;
						ptr++;
						i++;
					}
					aux[i] = '\0';
					uBloxGeoData.latMin = atoi(aux+i-2);

					aux[i-2] = '\0';
					uBloxGeoData.latDeg = atoi(aux);

					ptr++;
					i=0;
					while(*ptr != '.' && *ptr){
						aux[i] = *ptr;
						ptr++;
						i++;
					}
					aux[i] = 0;
					uBloxGeoData.latMinDec = atol(aux);

					break;
				case 1:	// Latitud Direction
					if (*ptr == ',')
						break;
					uBloxGeoData.latDir = *tok;
					break;
				case 2:	// Longitude
					ptr = tok;
					i=0;
					while(*ptr != '.'){
						aux[i] = *ptr;
						ptr++;
						i++;
					}
					aux[i] = '\0';
					uBloxGeoData.lonMin = atoi(aux+i-2);

					aux[i-2] = '\0';
					uBloxGeoData.lonDeg = atoi(aux);

					ptr++;
					i=0;
					while(*ptr != '.' && *ptr){
						aux[i] = *ptr;
						ptr++;
						i++;
					}
					aux[i] = 0;
					uBloxGeoData.lonMinDec = atol(aux);

					break;
				case 3:	// Longitude Direction
					uBloxGeoData.lonDir = *tok;
					break;
				case 4:	// Hora GMT
					ptr = tok;
					i = 0;
					while(*ptr != '.'){
						aux[i] = *ptr;
						ptr++;
						i++;
					}
					hora = atol(aux);
					uBloxGeoData.sec = hora % 100;
					hora = hora / 100;
					uBloxGeoData.min = hora %100;
					hora = hora / 100;
					uBloxGeoData.hour = hora % 100;
					break;
				case 5:
					uBloxGeoData.valid = (*tok == 'A');
					break;
			}


			dataIdx++;
			tok = strtok(0,",");

		}

	}
}
void uBloxProcessRTCM(void){
	uint16_t rtcmLen = (((uint16_t)headBuf[1]) & 0x03) *256 + headBuf[2];
	uint16_t rtcmID = (((uint16_t) bodyBuf[0]) * 256 + bodyBuf[1]) >> 4;

	if (RTKBroadcast(headBuf, RTCM_CHK_LEN) == 0){
		RTKBroadcast(bodyBuf, rtcmLen);
		RTKBroadcast(chkBuf, RTCM_CHK_LEN);
	}

	if (rtcmID == 0x3ED){
		rtcmState.cntRTCM1005++;

	}
	else if (rtcmID == 0x432){
		rtcmState.cntRTCM1074++;
	}
	else if (rtcmID == 0x43C){
		rtcmState.cntRTCM1084++;
	}
	else if (rtcmID == 0x446){
		rtcmState.cntRTCM1094++;
	}
	else if (rtcmID == 0x464){
		rtcmState.cntRTCM1124++;
	}
	else if (rtcmID == 0x4CE){
		rtcmState.cntRTCM1230++;
	}
	else if (rtcmID == 0xFE8){
		rtcmState.cntRTCM4072++;
	}
}




void uBloxProcessUBX(void){
	UBX_HEAD_STRUCT *ubxHeadStruct = (UBX_HEAD_STRUCT *) headBuf;

	// Base mode
	if (ubxHeadStruct->class == 0x01) {	// Navigation
		if (ubxHeadStruct->ID == 0x3B) {	// SVIN
			if (ubxHeadStruct->len == 40){
				memcpy(&navSvin,bodyBuf,40);
			}
		}
	}


	// Rover mode
	if (ubxHeadStruct->class == 0x01) {	// Navigation
		if (ubxHeadStruct->ID == 0x02) {	// Geodesic Position
			roverMsgs.NAV_POSLLH++;
		}
		else if (ubxHeadStruct->ID == 0x03) {	// Receiver Navigation status
			memcpy(&uBloxRoverStatus,bodyBuf,16);
			roverMsgs.NAV_STATUS++;
		}
		else if (ubxHeadStruct->ID == 0x07) {	// Position Velocity Time Solution
			roverMsgs.NAV_PVT++;
		}
		else if (ubxHeadStruct->ID == 0x3C) {	// Relative positioning info in NED Frame
			roverMsgs.NAV_RELPOSNED++;
		}
		else if (ubxHeadStruct->ID == 0x43) {	// Signal
			roverMsgs.NAV_SIG++;
		}
	}


	if (ubxHeadStruct->class == 0x02) {	// RXM
		if (ubxHeadStruct->ID == 0x32) {	// RXM-RTCM
			if (ubxHeadStruct->len == 8){
				memcpy(&rxmRtcm,bodyBuf,8);
				uint16_t rtcmID = rxmRtcm.msgType;
				if (rxmRtcm.crcFailed){
					rtcmState.crcError++;
					return;	// CRC Error!
				}

				if (rtcmID == 0x3ED){
					rtcmState.cntRTCM1005++;
				}
				else if (rtcmID == 0x432){
					rtcmState.cntRTCM1074++;
				}
				else if (rtcmID == 0x43C){
					rtcmState.cntRTCM1084++;
				}
				else if (rtcmID == 0x446){
					rtcmState.cntRTCM1094++;
				}
				else if (rtcmID == 0x464){
					rtcmState.cntRTCM1124++;
				}
				else if (rtcmID == 0x4CE){
					rtcmState.cntRTCM1230++;
				}
				else if (rtcmID == 0xFE8){
					rtcmState.cntRTCM4072++;
				}
			}
			roverMsgs.RXM_RTCM++;
		}
	}
}

void uBloxSendUBX(uint8_t * buf, uint8_t len){
	uint8_t sendBuff[64];
	uint8_t i;
	uint8_t chkA, chkB;

	// Preambulo
	sendBuff[0] = 0xB5;
	sendBuff[1] = 0x62;

	chkA = 0;
	chkB = 0;
	// Cuerpo
	for(i = 0; i < len; i++){
		sendBuff[i+2] = buf[i];
		chkA = chkA + buf[i];
		chkB = chkB + chkA;
	}
	// Cheksum

	sendBuff[i+2] = chkA;
	sendBuff[i+3] = chkB;

	UartSendBuffer(serialPort,sendBuff,len+4);
}

void uBloxValSet(uint32_t code, uint32_t value,uint8_t saveMode){
	uint8_t buf[17];
	uint8_t *cd = (uint8_t *) &code;
	uint8_t *val = (uint8_t *) &value;
	uint8_t len = 8;

	if ((cd[3] >> 4) == 1){
		if (value < 2)	// Boolean
			len = len + 1;
		else
			return;
	}
	else if ((cd[3] >> 4) == 2)
		len = len + 1;
	else if ((cd[3] >> 4) == 3)
		len = len + 2;
	else if ((cd[3] >> 4) == 4)
		len = len + 4;

	// Class
	buf[0] = 0x06;
	// ID
	buf[1] = 0x8A;

	// Len
	buf[2] = len;
	buf[3] = 0x00;

	// Version
	buf[4] = 0x00;
	// Bitfield
	buf[5] = saveMode;

	// Rsvd
	buf[6] = 0x00;
	buf[7] = 0x00;

	//MGOUT ID

	buf[8] = cd[0];
	buf[9] = cd[1];
	buf[10] = cd[2];
	buf[11] = cd[3];

	//Val
	buf[12] = value;

	buf[12] = val[0];
	buf[13] = val[1];
	buf[14] = val[2];
	buf[15] = val[3];



	uBloxSendUBX(buf, len + 4);

}



void uBloxEnableUart1Msg(uint16_t MsgType, uint8_t period){

	if (MsgType == 1005){
		uBloxValSet(0x209102BE,1,1);
	}
	else if (MsgType == 1074){
		uBloxValSet(0x2091035F,1,1);
	}
	else if (MsgType == 1084){
		uBloxValSet(0x20910364,1,1);
	}
	else if (MsgType == 1094){
		uBloxValSet(0x20910369,1,1);
	}
	else if (MsgType == 1124){
		uBloxValSet(0x2091036E,1,1);
	}
	else if (MsgType == 1230){
		uBloxValSet(0x20910304,1,1);
	}
	else if (MsgType == 4072){
		uBloxValSet(0x209102FF,1,1);
	}
	else {
		return;
	}
}

UBLOX_RTCM_STATE *UBloxGetRTCMSentences(void) {
	return &rtcmState;
}

UBLOX_NAV_SVIN_SATE *UBloxGetSurveyStatus(void) {
	return &navSvin;
}

UBLOX_ROVER_MSGS *UBloxGetRoverMsgs(void) {
	return &roverMsgs;
}

UBLOX_GEO *UBloxGetGeoData(void) {
	return &uBloxGeoData;
}


uint8_t UBloxRTCMProcesss(uint8_t rc){
	static uint16_t pending = 0;
	static uint8_t head[3];
	static uint8_t cnt = 0;

	if (!pending){
		head[cnt] = rc;
		cnt++;
		if (cnt == 0x01){
			if (rc != 0xD3){
				cnt = 0;
			}
		}
		if (cnt == 3){
			cnt = 0;
			pending = (head[1] & 0x03) * 256 + head[2] + 3;	// 3 del checksum
		}
	}
	else{
		pending--;
	}

	UartSendByte(serialPort, &rc);
	if (!pending && !cnt)
		return 1;
	else
		return 0;

}

UBLOX_UBX_STATUS *UBloxGetRoverStatus(void){
	return &uBloxRoverStatus;
}



static const uint32_t crc24qtab[256] = {
		0x000000, 0x864CFB, 0x8AD50D, 0x0C99F6, 0x93E6E1, 0x15AA1A, 0x1933EC, 0x9F7F17,
		0xA18139, 0x27CDC2, 0x2B5434, 0xAD18CF, 0x3267D8, 0xB42B23, 0xB8B2D5, 0x3EFE2E,
		0xC54E89, 0x430272, 0x4F9B84, 0xC9D77F, 0x56A868, 0xD0E493, 0xDC7D65, 0x5A319E,
		0x64CFB0, 0xE2834B, 0xEE1ABD, 0x685646, 0xF72951, 0x7165AA, 0x7DFC5C, 0xFBB0A7,
		0x0CD1E9, 0x8A9D12, 0x8604E4, 0x00481F, 0x9F3708, 0x197BF3, 0x15E205, 0x93AEFE,
		0xAD50D0, 0x2B1C2B, 0x2785DD, 0xA1C926, 0x3EB631, 0xB8FACA, 0xB4633C, 0x322FC7,
		0xC99F60, 0x4FD39B, 0x434A6D, 0xC50696, 0x5A7981, 0xDC357A, 0xD0AC8C, 0x56E077,
		0x681E59, 0xEE52A2, 0xE2CB54, 0x6487AF, 0xFBF8B8, 0x7DB443, 0x712DB5, 0xF7614E,
		0x19A3D2, 0x9FEF29, 0x9376DF, 0x153A24, 0x8A4533, 0x0C09C8, 0x00903E, 0x86DCC5,
		0xB822EB, 0x3E6E10, 0x32F7E6, 0xB4BB1D, 0x2BC40A, 0xAD88F1, 0xA11107, 0x275DFC,
		0xDCED5B, 0x5AA1A0, 0x563856, 0xD074AD, 0x4F0BBA, 0xC94741, 0xC5DEB7, 0x43924C,
		0x7D6C62, 0xFB2099, 0xF7B96F, 0x71F594, 0xEE8A83, 0x68C678, 0x645F8E, 0xE21375,
		0x15723B, 0x933EC0, 0x9FA736, 0x19EBCD, 0x8694DA, 0x00D821, 0x0C41D7, 0x8A0D2C,
		0xB4F302, 0x32BFF9, 0x3E260F, 0xB86AF4, 0x2715E3, 0xA15918, 0xADC0EE, 0x2B8C15,
		0xD03CB2, 0x567049, 0x5AE9BF, 0xDCA544, 0x43DA53, 0xC596A8, 0xC90F5E, 0x4F43A5,
		0x71BD8B, 0xF7F170, 0xFB6886, 0x7D247D, 0xE25B6A, 0x641791, 0x688E67, 0xEEC29C,
		0x3347A4, 0xB50B5F, 0xB992A9, 0x3FDE52, 0xA0A145, 0x26EDBE, 0x2A7448, 0xAC38B3,
		0x92C69D, 0x148A66, 0x181390, 0x9E5F6B, 0x01207C, 0x876C87, 0x8BF571, 0x0DB98A,
		0xF6092D, 0x7045D6, 0x7CDC20, 0xFA90DB, 0x65EFCC, 0xE3A337, 0xEF3AC1, 0x69763A,
		0x578814, 0xD1C4EF, 0xDD5D19, 0x5B11E2, 0xC46EF5, 0x42220E, 0x4EBBF8, 0xC8F703,
		0x3F964D, 0xB9DAB6, 0xB54340, 0x330FBB, 0xAC70AC, 0x2A3C57, 0x26A5A1, 0xA0E95A,
		0x9E1774, 0x185B8F, 0x14C279, 0x928E82, 0x0DF195, 0x8BBD6E, 0x872498, 0x016863,
		0xFAD8C4, 0x7C943F, 0x700DC9, 0xF64132, 0x693E25, 0xEF72DE, 0xE3EB28, 0x65A7D3,
		0x5B59FD, 0xDD1506, 0xD18CF0, 0x57C00B, 0xC8BF1C, 0x4EF3E7, 0x426A11, 0xC426EA,
		0x2AE476, 0xACA88D, 0xA0317B, 0x267D80, 0xB90297, 0x3F4E6C, 0x33D79A, 0xB59B61,
		0x8B654F, 0x0D29B4, 0x01B042, 0x87FCB9, 0x1883AE, 0x9ECF55, 0x9256A3, 0x141A58,
		0xEFAAFF, 0x69E604, 0x657FF2, 0xE33309, 0x7C4C1E, 0xFA00E5, 0xF69913, 0x70D5E8,
		0x4E2BC6, 0xC8673D, 0xC4FECB, 0x42B230, 0xDDCD27, 0x5B81DC, 0x57182A, 0xD154D1,
		0x26359F, 0xA07964, 0xACE092, 0x2AAC69, 0xB5D37E, 0x339F85, 0x3F0673, 0xB94A88,
		0x87B4A6, 0x01F85D, 0x0D61AB, 0x8B2D50, 0x145247, 0x921EBC, 0x9E874A, 0x18CBB1,
		0xE37B16, 0x6537ED, 0x69AE1B, 0xEFE2E0, 0x709DF7, 0xF6D10C, 0xFA48FA, 0x7C0401,
		0x42FA2F, 0xC4B6D4, 0xC82F22, 0x4E63D9, 0xD11CCE, 0x575035, 0x5BC9C3, 0xDD8538
};

uint32_t crc24q(const uint8_t *buf, uint32_t len, uint32_t crc) {
	uint32_t i;
	for (i = 0; i < len; i++)
		crc = ((crc << 8) & 0xFFFFFF) ^ crc24qtab[(crc >> 16) ^ buf[i]];
	return crc;
}
