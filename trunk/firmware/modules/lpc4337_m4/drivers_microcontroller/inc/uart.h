/* Copyright 2016, 
 * Leandro D. Medus
 * lmedus@bioingenieria.edu.ar
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 * Sebastián Mateos
 * sebastianantoniomateos@gmail.com
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */


#ifndef UART_H
#define UART_H

/** \brief UART Bare Metal driver for the peripheral in the EDU-CIAA Board.
 **
 ** This is a driver to control the UART present in the EDU-CIAA Board.
 **
 **/

/** \addtogroup Drivers_Programable Drivers Programable
 ** @{ */
/** \addtogroup Drivers_Microcontroller Drivers microcontroller
 ** @{ */
/** \addtogroup UART UART
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 *	LM			Leandro Medus
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20160610 v0.1 initials initial version leo
 */

/*==================[inclusions]=============================================*/
#include "stdint.h"
#include "chip.h"
//#include "srting.h"

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

#define SERIAL_PORT_PC				0
#define SERIAL_PORT_P2_CONNECTOR	1
#define SERIAL_PORT_RS485			2
#define SERIAL_PORT_GPIO_1_2		3
#define SERIAL_PORT_TEC_F1_F2		4


#define NO_INT	0		/*NULL pointer for no interrupt port handler*/

typedef enum {
	UART2_DEF = SERIAL_PORT_PC,
	UART3_DEF = SERIAL_PORT_P2_CONNECTOR,
	UART0_DEF = SERIAL_PORT_RS485,
	UART0_ALT = SERIAL_PORT_GPIO_1_2,
	UART3_ALT = SERIAL_PORT_TEC_F1_F2
}UART_PORT;

typedef struct {				/*!< Serial Ports Struct*/
	UART_PORT uartPort;			/*!< port: FTDI, RS232, RS485*/
	uint32_t baud_rate;			/*!< Baud Rate*/
	void *pSerial;				/*!< Function pointer to app serial port*/
} SERIAL_PORT;

typedef struct {				//!< Serial Ports Struct for RingBuffer
	SERIAL_PORT serialPort;		//!< A SERIAL_PORT struct
	RINGBUFF_T *RxRingBuff;		//!< Ring buffer for reception
	void *rxBuffer;				//!< Reception buffer pointer
	int rxBuffSize;				//!< Reception buffer size
	RINGBUFF_T *TxRingBuff;		//!< Ring buffer for transmission
	void *txBuffer;				//!< Transmission buffer pointer
	int txBuffSize;				//!< Transmission buffer size
} SERIAL_PORT_RB;



/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

/** @fn uint32_t UartInit(serial_config *port)
 * @brief Inicializar UART
 * @param[in] port Puerto para inicializar (FTDI, RS232, RS485)
 */
uint32_t UartInit(SERIAL_PORT *port);

/** @fn uint32_t UartReadStatus(uint8_t port)
 * @brief Leer el estado del puerto
 * @param[in] port Puerto que se desea leer
 */
uint32_t UartReadStatus(SERIAL_PORT port);

/** @fn uint32_t UartRxReady(uint8_t port)
 * @brief Leer el estado de recepcion del puerto
 * @param[in] port Puerto que se desea leer
 */
uint32_t UartRxReady(SERIAL_PORT port);

/** @fn uint32_t UartReadByte(uint8_t port)
 * @brief Lee un byte del puerto serie
 * @param[in] port Puerto del que se desea leer
 * @param[in] dat Puntero donde se colocara el dato
 * @return true si el dato se pudo leer - flase en caso contrario
 */
uint8_t UartReadByte(SERIAL_PORT port, uint8_t* dat);

/** @fn void UartSendByte(SERIAL_PORT port,uint8_t* dat)
 * @brief Envia un byte por el puerto serie
 * @param[in] port Puerto por el que se desea transmitir
 * @param[in] dat Puntero al dato a transmitir
 */
void UartSendByte(SERIAL_PORT port,uint8_t* dat);

/** @fn void UartSendString(SERIAL_PORT port,char* msg)
 * @brief Envia un string terminado en cero por puerto serie
 * @param[in] port Puerto por el que se desea transmitir
 * @param[in] dat Puntero al string
 */
void UartSendString(SERIAL_PORT port,char* msg);/*Stops on '\0'*/

/** @fn void UartSendBuffer(SERIAL_PORT port, const void *data, uint8_t nbytes)
 * @brief Envia un bafer de datos de longitud determinada
 * @param[in] port Puerto por el que se desea transmitir
 * @param[in] data Puntero al buffer
 * @param[in] nbytes tamaño del buffer
 */
void UartSendBuffer(SERIAL_PORT port, const void *data, uint8_t nbytes);  /*Send n Bytes - Inline candidate...*/


// Ring Buffer UART

/** @fn uint32_t UartInitRB(SERIAL_PORT_RB *port)
 * @brief Inicializa UART utilizando ring buffer
 * @param[in] port Puerto para inicializar con los datos de ring buffer asociados
 */
uint32_t UartInitRB(SERIAL_PORT_RB *port);

/** @fn void UartSendByteRB(SERIAL_PORT port,uint8_t* dat)
 * @brief Envia un byte por el puerto serie utilizando un ringbuffer
 * @param[in] port Puerto por el que se desea transmitir
 * @param[in] dat Puntero al dato a transmitir
 */
void UartSendByteRB(SERIAL_PORT_RB port, void *data);

/** @fn void UartSendBufferRB(SERIAL_PORT port, const void *data, uint8_t nbytes)
 * @brief Envia un bafer de datos de longitud determinada utilizando un ringbuffer
 * @param[in] port Puerto por el que se desea transmitir
 * @param[in] data Puntero al buffer
 * @param[in] len tamaño del buffer
 */
uint32_t UartSendBufferRB(SERIAL_PORT_RB port, void *data, uint16_t len);

/** @fn uint32_t UartReadByteRB(uint8_t port)
 * @brief Lee un byte del puerto serie utilizando un ringbuffer
 * @param[in] port Puerto del que se desea leer
 * @param[in] dat Puntero donde se colocara el dato
 * @return true si el dato se pudo leer - flase en caso contrario
 */
uint8_t UartReadByteRB(SERIAL_PORT_RB port, void *data);

/** @fn void UartSendString(SERIAL_PORT port,char* msg)
 * @brief Envia un string terminado en cero por puerto serie utilizando un ringbuffer
 * @param[in] port Puerto por el que se desea transmitir
 * @param[in] dat Puntero al string
 */
void UartSendStringRB(SERIAL_PORT_RB port, char *msg);


/** @fn void UART_IRQRBHandler(SERIAL_PORT_RB port);
 * @brief Handler necesario en la implementacion de ring buffers
 * @param[in] port Puerto serie con los datos del ringbuffer
 */
void UART_IRQRBHandler(SERIAL_PORT_RB port);

/** @fn char* UartItoa(uint32_t val, uint8_t base)
 * @brief Conversor de entero a ASCII
 * @param[in] val Valor entero que se desea convertir
 * @param[in] base Base sobre la cual se desea realizar la conversion
 * @return Puntero al primer elemento de la cadena convertida
 */
char *UartItoa(uint32_t val, uint8_t base);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
#endif /* #ifndef UART_H */

