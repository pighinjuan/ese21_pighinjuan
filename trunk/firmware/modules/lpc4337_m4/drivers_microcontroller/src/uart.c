/* Copyright 2016, 
 * Eduardo Filomena
 * efilomena@bioingenieria.edu.ar
 * Juan Manuel Reta
 * jmrera@bioingenieria.edu.ar
 * Sebastián Mateos
 * smateos@ingenieria.uner.edu.ar	
 * Facultad de Ingeniería
 * Universidad Nacional de Entre Ríos
 * Argentina
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief Bare Metal driver for uart in the EDU-CIAA board.
 **
 **/

/*==================[inclusions]=============================================*/
#include "uart.h"
#include <string.h>

/*==================[macros and definitions]=================================*/

#define DELAY_CHARACTER 500000

/* UART0 (RS485/Profibus) */

#define RS485_TXD_MUX_GROUP   9
#define RS485_RXD_MUX_GROUP   9

#define RS485_TXD_MUX_PIN   5
#define RS485_RXD_MUX_PIN   6

/* UART0 (GPIO1/GPIO2)*/
#define GPIO_1_2_TXD_MUX_GROUP   6
#define GPIO_1_2_RXD_MUX_GROUP   6

#define GPIO_1_2_TXD_MUX_PIN   4
#define GPIO_1_2_RXD_MUX_PIN   5

#define GPIO_1_2_MUX_PIN_FUNC   2


/* UART2 (USB-UART) */
#define UART_USB_TXD_MUX_GROUP   7
#define UART_USB_RXD_MUX_GROUP   7

#define UART_USB_TXD_MUX_PIN   1
#define UART_USB_RXD_MUX_PIN   2


/* UART3 (RS232) */

#define RS232_TXD_MUX_GROUP   2
#define RS232_RXD_MUX_GROUP   2

#define RS232_TXD_MUX_PIN   3
#define RS232_RXD_MUX_PIN   4

/* UART3 (TEC_F1/TEC_F2)*/
#define TEC_F1_TXD_MUX_GROUP   4
#define TEC_F2_RXD_MUX_GROUP   4

#define TEC_F1_TXD_MUX_PIN    1
#define TEC_F2_RXD_MUX_PIN    2

#define TEC_F2_MUX_PIN_FUN    6



/*Direction Pin*/
#define DIR_RS485_MUX_GROUP 	6
#define DIR_RS485_MUX_PIN 	2

#define UART0 0
#define UART2 2
#define UART3 3

/*==================[internal data declaration]==============================*/
typedef struct {
	uint8_t uart;		//!< hardware Port
	uint8_t txGroup;		//!< hardware Port
	uint8_t rxGroup;		//!< hardware Port
	uint8_t txPin;		//!< Hardware Pin
	uint8_t rxPin;		//!< Hardware Pin
	uint8_t txFunc;		//!< TxPin Func
	uint8_t rxFunc;		//!< RxPin Func
}UART_HW_SETUP;

/*==================[internal functions declaration]=========================*/

void (*UART2IntHandler)()= NO_INT;
void (*UART3IntHandler)()= NO_INT;
void (*UART0IntHandler)()= NO_INT;

/*==================[internal data definition]===============================*/
const LPC_USART_T* uartPorts[] = {LPC_USART0,0,LPC_USART2,LPC_USART3};

const UART_HW_SETUP uarHWConfig[] = {
		{UART2,7,7,1,2,6,6},
		{UART3,2,2,3,4,2,2},
		{UART0,9,9,5,6,7,7},
		{UART0,6,6,4,5,2,2},
		{UART3,4,4,1,2,6,6},
};

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/
LPC_USART_T* GetUARTPort(SERIAL_PORT port);
LPC_USART_T* GetUARTPortRB(SERIAL_PORT_RB port);

/*==================[external functions definition]==========================*/
/** \brief UART Initialization method  */
uint32_t UartInit(SERIAL_PORT *port) {
	UART_HW_SETUP hwSetup = uarHWConfig[port->uartPort];

	LPC_USART_T * uartPort = (LPC_USART_T *) uartPorts[hwSetup.uart];

	Chip_UART_Init(uartPort);
	Chip_UART_SetBaud(uartPort, port->baud_rate);

	Chip_UART_SetupFIFOS(uartPort, UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0);

	Chip_UART_TXEnable(uartPort);

	Chip_SCU_PinMux(hwSetup.txGroup, 	hwSetup.txPin,	MD_PDN,					hwSetup.txFunc);    /* P7_1: UART2_TXD */
	Chip_SCU_PinMux(hwSetup.rxGroup, 	hwSetup.rxPin, 	MD_PLN|MD_EZI|MD_ZI,	hwSetup.rxFunc); 	/* P7_2: UART2_RXD */

	if (port->uartPort == SERIAL_PORT_RS485){
		Chip_UART_SetRS485Flags(LPC_USART0, UART_RS485CTRL_DCTRL_EN | UART_RS485CTRL_OINV_1);
		Chip_SCU_PinMux(DIR_RS485_MUX_GROUP, DIR_RS485_MUX_PIN, MD_PDN, FUNC2);              		/* P6_2: UART0_DIR */
	}


	switch(hwSetup.uart) {
		case UART0:
			UART0IntHandler = port->pSerial;
			if(UART0IntHandler != NO_INT) {
				Chip_UART_IntEnable(uartPort, UART_IER_RBRINT);
				NVIC_EnableIRQ(USART0_IRQn);
			}
			break;
	 	case UART2:
	 		UART2IntHandler = port->pSerial;
			if(UART2IntHandler != NO_INT) {
				Chip_UART_IntEnable(uartPort, UART_IER_RBRINT);
				NVIC_EnableIRQ(USART2_IRQn);
			}
	 		break;
	 	case UART3:
	 		UART3IntHandler = port->pSerial;
	 		if(UART3IntHandler != NO_INT) {
				Chip_UART_IntEnable(uartPort, UART_IER_RBRINT);
				NVIC_EnableIRQ(USART3_IRQn);
			}
	 		break;
	}
	return TRUE;
}

uint32_t UartInitRB(SERIAL_PORT_RB *port) {
	RingBuffer_Init(port->RxRingBuff,port->rxBuffer,1,port->rxBuffSize);
	RingBuffer_Init(port->TxRingBuff,port->txBuffer,1,port->txBuffSize);

	UartInit(&port->serialPort);
	return TRUE;
}

uint32_t UartReadStatus(SERIAL_PORT port) {
	LPC_USART_T * uartPort = GetUARTPort(port);

	return Chip_UART_ReadLineStatus(uartPort) & UART_LSR_THRE;

}
uint32_t UartRxReady(SERIAL_PORT port) {
	LPC_USART_T * uartPort = GetUARTPort(port);

	return Chip_UART_ReadLineStatus(uartPort) & UART_LSR_RDR;
}

uint8_t UartReadByte(SERIAL_PORT port, uint8_t* dat) {
	LPC_USART_T * uartPort = GetUARTPort(port);

	if(UartRxReady(port)) {
		*dat = Chip_UART_ReadByte(uartPort);
		return TRUE;
	}
	else {
		return FALSE;
	}
}

uint8_t UartReadByteRB(SERIAL_PORT_RB port, void *data) {
	LPC_USART_T * uartPort = GetUARTPortRB(port);

	return (Chip_UART_ReadRB(uartPort,port.RxRingBuff,data,1) == 1);

}



void UartSendByteRB(SERIAL_PORT_RB port, void *data) {
	LPC_USART_T * uartPort = GetUARTPortRB(port);

	Chip_UART_SendRB(uartPort, port.TxRingBuff, data, 1);

	return ;

}

uint32_t UartSendBufferRB(SERIAL_PORT_RB port, void *data, uint16_t len) {
	LPC_USART_T * uartPort = GetUARTPortRB(port);

	return Chip_UART_SendRB(uartPort, port.TxRingBuff, data, len);

}

void UartSendByte(SERIAL_PORT port,uint8_t* dat) {
	LPC_USART_T * uartPort = GetUARTPort(port);

	while(UartReadStatus(port) == 0);

	Chip_UART_SendByte(uartPort, *dat);
}

void UartSendString(SERIAL_PORT port, char *msg) {/*Stops on '\0'*/

	UartSendBuffer(port, msg, strlen(msg));
}

void UartSendStringRB(SERIAL_PORT_RB port, char *msg) {/*Stops on '\0'*/
	UartSendBufferRB(port, msg, strlen(msg));
}



void UartSendBuffer(SERIAL_PORT port, const void *data, uint8_t nbytes) { /*Send n Bytes - Inline candidate...*/

	LPC_USART_T * uartPort = GetUARTPort(port);

	Chip_UART_SendBlocking(uartPort,data, nbytes);
}

char *UartItoa(uint32_t val, uint8_t base) {
	static char buf[32] = {0};

	uint32_t i = 30;

	for(; val && i ; --i, val /= base)
		buf[i] = "0123456789abcdef"[val % base];

	if (i == 30){
		buf[31] = '\0';
		buf[30] = '0';
		i--;
	}

	return &buf[i+1];
}

void UART0_IRQHandler(void) {

	if(UART0IntHandler != NO_INT) {
		UART0IntHandler();
	}
}

void UART2_IRQHandler(void) {
	if(UART2IntHandler != NO_INT) {
		UART2IntHandler();
	}
}

void UART3_IRQHandler(void) {
	if(UART3IntHandler != NO_INT) {
		UART3IntHandler();
	}
}

void UART_IRQRBHandler(SERIAL_PORT_RB port) {
	LPC_USART_T * uartPort = GetUARTPortRB(port);


	Chip_UART_IRQRBHandler(uartPort, port.RxRingBuff, port.TxRingBuff);
}


LPC_USART_T* GetUARTPort(SERIAL_PORT port) {
	return (LPC_USART_T *) uartPorts[uarHWConfig[port.uartPort].uart];
}
LPC_USART_T* GetUARTPortRB(SERIAL_PORT_RB port) {

	return GetUARTPort(port.serialPort);
}


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
